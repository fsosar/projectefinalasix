<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Member;
use App\Entity\MemberRole;
use DateTime;
use League\OAuth2\Client\Provider\Google;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SecurityController extends Controller
{
    /**
     * @Route("/{_locale}/login-check", name="app_logincheck")
     */
    public function loginCheckAction(Request $request)
    {
        // return new response(true);
    }

    /**
     * @Route("/{_locale}/login", name="app_login")
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $locale = $request->get('_locale');
        // Guardamos en una sessión el idioma

        $request->getSession()->set('locale', $locale);

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        if ($error) {
            $member = $this->getDoctrine()->getManager()->getRepository(Member::class)->findOneBy(['username' => $error->getToken()->getUser()]);

            $date = new \DateTime('now');
            $activity = new Activity();
            if ($member) {
                $activity->setIdMember($member->getIdMember());
            } else {
                $params = ['username' => $error->getToken()->getUser()];
                $activity->setParams(json_encode($params, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK));
            }
            $activity->setRoute('login_attempt_fail');
            $activity->setDate($date->getTimestamp());
            $activity->setLocale($locale);
            $this->getDoctrine()->getManager()->persist($activity);
            $this->getDoctrine()->getManager()->flush();
        }
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/{_locale}/register", name="app_register")
     */
    public function registerAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $locale = $request->get('_locale');
        // Guardamos en una sessión el idioma

        $request->getSession()->set('locale', $locale);

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        if ($error) {
            $member = $this->getDoctrine()->getManager()->getRepository(Member::class)->findOneBy(['username' => $error->getToken()->getUser()]);

            $date = new \DateTime('now');
            $activity = new Activity();
            if ($member) {
                $activity->setIdMember($member->getIdMember());
            } else {
                $params = ['username' => $error->getToken()->getUser()];
                $activity->setParams(json_encode($params, JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK));
            }
            $activity->setRoute('login_attempt_fail');
            $activity->setDate($date->getTimestamp());
            $activity->setLocale($locale);
            $this->getDoctrine()->getManager()->persist($activity);
            $this->getDoctrine()->getManager()->flush();
        }
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/register.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/{_locale}/logout", name="app_logout")
     */
    public function logoutAction(Request $request)
    {
        return new response(true);
    }

    /**
     * @Route("/{_locale}/api/member/activate/{secretCode}")
     */
    public function apiActivateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');

        $secretCode = $helper->sanitize($request->get('secretCode'));
        $dateNow = new DateTime('now');

        $validado = 0;
        $expired = 0;
        $member = $em->getRepository(Member::class)->findOneBy(['emailSecretCode' => $secretCode]);
        if ($member) {
            $registered = $member->getCreationDate();
            $dateValid = $member->getCreationDate() + 259200;

            if ($dateValid >= $dateNow->getTimestamp()) {
                $member->setEmailValidated(1);
                $member->setEmailSecretCode(null);
                $em->persist($member);
                $em->flush();
                $validado = 1;
            } else {
                $expired = 1;
            }
        }

        if (!$expired && !$validado) {
            throw new NotFoundHttpException();
        }

        return $this->render('security/validate_email.html.twig', [
            'expired' => $expired,
            'validado' => $validado
        ]);
    }

    /**
     * @Route("/{_locale}/member/resend")
     */
    public function activateResendAction(Request $request)
    {
        return $this->render('security/validate_resend.html.twig');
    }

    /**
     * @Route("/{_locale}/member/activate-username/{emailSecretCode}/{newUsername}")
     */
    public function apiActivateUsernameAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');

        $emailSecretCode = $helper->sanitize($request->get('emailSecretCode'));
        $newUsername = $helper->sanitize($request->get('newUsername'));

        $member = $em->getRepository(Member::class)->findOneBy(['emailSecretCode' => $emailSecretCode]);
        $memberEmail = $em->getRepository(Member::class)->findOneBy(['username' => $newUsername, 'deleted' => 0]);
        $dateNow = new DateTime('now');
        $dateValid = $member->getChangeUsernameDate() + 259200;

        $validado = 0;
        $expired = 1;
        if ($dateValid >= $dateNow->getTimestamp() && $member && $member->getChangeUsername() == $newUsername && !$memberEmail) {
            $member->setEmailValidated(1);
            $member->setUsername($newUsername);
            $member->setChangeUsername(null);
            $member->setEmailSecretCode(null);
            $member->setChangeUsernameDate(null);
            $em->persist($member);
            $em->flush();

            $validado = 1;
            $expired = 0;
        }

        return $this->render('security/validate_email.html.twig', [
            'expired' => $expired,
            'validado' => $validado
        ]);
    }

    /**
     * @Route("/{_locale}/member/resend-activation")
     */
    public function emailResendAction(Request $request)
    {
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');
        $em = $this->getDoctrine()->getManager();

        $username = $helper->sanitize($request->request->get('username'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $responseMsgError = [];
        $response = new JsonResponse();
        $date = new DateTime('now');

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                array_push($responseMsgError, 'El email introducido es invalido.');
            }

            if (empty($responseMsgError)) {
                $changeUsername = true;
                // Comprobar si lo que quiere es validar el nuevo email
                $member = $em->getRepository(Member::class)->findOneBy([
                    'changeUsername' => $username
                ]);

                if (!$member) {
                    $changeUsername = false;
                    // En caso que no quiere validad nuevo email, comprobar si quiere validar su email actual
                    $member = $em->getRepository(Member::class)->findOneBy([
                        'username' => $username,
                        'emailValidated' => 0
                    ]);
                }

                if (!$member) {
                    // En el caso de que no tengamos que validar detectamos si el motivo es por que no existe el usuario
                    $member = $em->getRepository(Member::class)->findOneBy([
                        'username' => $username
                    ]);
                    if (!$member) {
                        array_push($responseMsgError, 'Este usuario no existe.');
                    } else {
                        array_push($responseMsgError, 'Esta cuanta ya esta validada.');
                    }
                } else {
                    $emailSecretCode = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true) . $member->getIdMember());

                    $member->setEmailSecretCode($emailSecretCode);
                    if ($changeUsername) {
                        $member->setChangeUsername($username);
                        $member->setChangeUsernameDate($date->getTimestamp());
                    }

                    $em->persist($member);
                    $em->flush();

                    if ($changeUsername) {
                        $body = $this->renderView('templates/emails/member/verification_mail.html.twig', [
                            'member' => $member,
                            'url' => $this->generateUrl('app_security_apiactivateusername', ['emailSecretCode' => $emailSecretCode, 'newUsername' => $username])
                        ]);
                    } else {
                        $body = $this->renderView('templates/emails/member/register_member.html.twig', [
                            'url' => $this->generateUrl('app_security_apiactivate', ['secretCode' => $emailSecretCode])
                        ]);
                    }
                    $manager_mail->templeteOne([
                        'to' => $username,
                        'subject' => 'Email de verificación',
                        'body' => $body
                    ]);
                }
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Hubo un error al reenviar el email, vuelve a intentarlo más tarde.');
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Comprueba tu email!'
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError
            ]);
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/member/recover-password")
     */
    public function recoverPasswordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');

        $username = $helper->sanitize($request->request->get('recuperarEmail'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $dateNow = new DateTime('now');
        $response = new JsonResponse();
        $responseMsgError = [];

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            if (empty($responseMsgError)) {
                // Comprobar si el email existe
                $member = $this->getDoctrine()->getManager()->getRepository(Member::class)->findOneBy([
                    'username' => $username, 
                    'validated' => 1, 
                    'deleted' => 0,
                    'isActive' => 1
                ]);

                if (!$member) {
                    array_push($responseMsgError, 'No existe ninguna cuanta con este correo.');
                }

                if ($member) {
                    $emailSecretCode = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true) . $member->getIdMember());
                    $member->setEmailSecretCode($emailSecretCode);
                    $em->persist($member);
                    $em->flush();

                    // Anotar la actividad
                    $activity = new Activity();
                    $activity->setIdMember($member->getIdMember());
                    $activity->setRoute('password_recovery_email');
                    $activity->setDate($dateNow->getTimestamp());
                    $em->persist($activity);
                    $em->flush();

                    // Enviar mail
                    $manager_mail->templeteOne([
                        'to' => $username,
                        'subject' => 'Le Facilitamos sus datos de acceso',
                        'body' =>
                            $this->renderView('templates/emails/member/recover_password.html.twig', [
                                'secretCode' => $member->getEmailSecretCode()
                        ])
                    ]);
                }
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Hubo un error al enviar el email de recuperación, vuelve a intentarlo más tarde.');
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Comprueba tu email!'
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError
            ]);
        }
        return $response;
    }

    /**
     * @Route("/{_locale}/member/recovery/{secretCode}")
     */
    public function passwordRecoveryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');

        $secretCode = $helper->sanitize($request->get('secretCode'));

        $dateNow = new DateTime('now');
        $validado = 0;
        $expired = 0;

        $member = $em->getRepository(Member::class)->findOneBy(['emailSecretCode' => $secretCode]);
        if ($member) {
            $username = $member->getUsername();
            $activity = $em->getRepository(Activity::class)->findBy([
                'route' => 'password_recovery_email',
                'idMember' => $member->getIdMember()
            ]);

            foreach ($activity as $item) {
                $dateValid = $item->getDate() + 259200;
                if ($dateValid >= $dateNow->getTimestamp()) {
                    $validado = 1;
                }
            }
        }

        return $this->render('security/password_recovery.html.twig', [
            'expired' => $expired,
            'validado' => $validado,
            'member' => $member
        ]);

    }

    /**
     * @Route("/{_locale}/api/member/recovery")
     */
    public function apiPasswordRecoveryAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');

        $password = $helper->sanitize($request->request->get('new-password'));
        $passwordRepeat = $helper->sanitize($request->request->get('passwordRepeat'));
        $username = $helper->sanitize($request->request->get('username'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $responseMsgError = [];
        $response = new JsonResponse();
        $dateNow = new DateTime('now');

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            if (empty($responseMsgError)) {
                if ($password != $passwordRepeat) {
                    array_push($responseMsgError, 'Cuidado! Los passwords no coinciden!');
                } else {
                    $member = $em->getRepository(Member::class)->findOneBy(['username' => $username]);
                    $activity = $em->getRepository(Activity::class)->findOneBy([
                        'route' => 'password_recovery_email',
                        'idMember' => $member->getIdMember()
                    ]);
                    $dateValid = $activity->getDate() + 259200;
                    if ($member && $member->getEmailSecretCode() && $dateValid >= $dateNow->getTimestamp()) {
                        $pwd = $encoder->encodePassword($member, $password);

                        $member->setPassword($pwd);
                        $member->setEmailValidated(1);
                        $member->setEmailSecretCode(null);
                        $em->persist($member);
                        $em->flush();

                        // Anotar la actividad
                        $activity = new Activity();
                        $activity->setIdMember($member->getIdMember());
                        $activity->setRoute('password_recovery_changed');
                        $activity->setDate($dateNow->getTimestamp());
                        $em->persist($activity);
                        $em->flush();
                    }}
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Hubo un error al poner la nueva contraseña.');
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Enhorabuena, tu password se ha cambiado!'
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError
            ]);
        }
        return $response;
    }

    /**
     * @Route("/api/login/social-login-register", name="app_social")
     */
    public function apiLoginSocialNetworkAction(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $em = $this->getDoctrine()->getManager();

        $helper = $this->get('app.helper');
        $response = new JsonResponse();

        $redSocial = $helper->sanitize($request->get('redSocial'));
        $id = $helper->sanitize($request->get('id'));
        $name = $helper->sanitize($request->get('name'));
        $email = $helper->sanitize($request->get('email'));
        $role = $helper->sanitize($request->get('role'));

        if ($id && $name && $email) {

            // ROL DEL USUARIO
            if ($role != 2 && $role != 3) {
                $role = 3;
            }
            $memberRole = $em->getRepository(MemberRole::class)->findOneBy(['idMemberRole' => $role]);

            // Comprobar si el email ya existe en la ddbb
            $member = $em->getRepository(Member::class)->findOneBy(['username' => $email]);

            // Si en la ddbb no existe este id generamos un usuario con el id
            // Buscamos si el id Existe
            // Si el usuario ya existe, comprobamos si el idFacebook o idGoogle que nos pase tamien existe, si no lo actualizamos
            switch ($redSocial) {
                case 'facebook':
                    $idExist = $em->getRepository(Member::class)->findOneBy(['keyFacebook' => $id]);
                    break;
                case 'google':
                    $idExist = $em->getRepository(Member::class)->findOneBy(['keyGoogle' => $id]);
                    break;
            }

            $registrarActualizarUsuario = 0;
            // Si el email (usuario) existe entramos
            if ($member) {
                // Comprobamos si el idFacebook o idGoogle existen
                if (!$idExist) {
                    // Como no existe actualizamos el usuario
                    $registrarActualizarUsuario = 1;
                    $member = $em->getRepository(Member::class)->findOneBy(['username' => $email]);
                }

                // Como el email no existe comprobamos si el idFacebook o idGoogle existen
            } else if (!$idExist) {
                // Como no existe email ni el idFacebook ni idGoogle creamos otro usuario
                $registrarActualizarUsuario = 1;
                $member = new Member();

                $member->setIdMemberRole($memberRole);

                $member->setName($name);
                $member->setUsername($email);
                $member->setDeleted(0);
                $member->setValidated(1);
                $member->setEmailValidated(1);
                $member->setCreationDate(time());

                // Cifrar la password
                $password = 'PASSWORD';
                $pwd = $encoder->encodePassword($member, $password);
                $member->setPassword($pwd);

                if (!file_exists('member/')) {
                    mkdir('member/');
                }
                if (!file_exists('member/' . $member->getIdMember())) {
                    mkdir('member/' . $member->getIdMember());
                }

                if (!file_exists('member/' . $member->getIdMember() . '/img-avatar/')) {
                    mkdir('member/' . $member->getIdMember() . '/img-avatar/');
                }
            }

            // Entramos en el caso de que se tenga que actualizar o crear el usuario
            if ($registrarActualizarUsuario == 1) {

                $member->setUsername($email);

                // Guardamos el id dependiendo del botón de la red
                switch ($redSocial) {
                    case 'facebook':
                        $member->setKeyFacebook($id);
                        break;
                    case 'google':
                        $member->setKeyGoogle($id);
                        break;
                }

                $em->persist($member);
                $em->flush();

            }

            $member = null;

            // ### Login
            switch ($redSocial) {
                case 'facebook':
                    $member = $em->getRepository(Member::class)->findOneBy(['validated' => '1', 'deleted' => '0', 'keyFacebook' => $id]);
                    break;
                case 'google':
                    $member = $em->getRepository(Member::class)->findOneBy(['validated' => '1', 'deleted' => '0', 'keyGoogle' => $id]);
                    break;
            }

            if ($member) {

                $firewall_name = 'our_db_provider';

                $token = new UsernamePasswordToken($member, null, $firewall_name, $member->getRoles());

                $this->get('security.token_storage')->setToken($token);

                return $this->redirectToRoute('dashboard');

            } else {

                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Usuari que no existex'
                ]);

                return $response;

            }
        }

        $response->setStatusCode(500);
        $response->setData([
            'response' => 'error',
            'msg' => 'Error inesperat'
        ]);
        return $response;

    }

    /**
     * @Route("/api/member/google")
     */
    public function apiLoginRegisterGoogleAction(Request $request, UserPasswordEncoderInterface $encoder)
    {

        $locale = $request->get('_locale');
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $response = new JsonResponse();

        $provider = new Google([
            'clientId' => '657456729909-quigd4gj4pbn458p6t2erpt0aj4vpg45.apps.googleusercontent.com',
            'clientSecret' => 'K2WSuq_dKitOCCVJk1CfuryM',
            'redirectUri' => 'https://owsbase.devplatform.tech/api/member/google',
        ]);
        try {

            if (!empty($_GET['error'])) {
                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Got error: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8')
                ]);
                // Got an error, probably user denied access
                return $response;

            } elseif (empty($_GET['code'])) {

                // If we don't have an authorization code then get one
                $authUrl = $provider->getAuthorizationUrl();
                $_SESSION['oauth2state'] = $provider->getState();
                header('Location: ' . $authUrl);
                exit;

            } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

                // State is invalid, possible CSRF attack in progress
                unset($_SESSION['oauth2state']);
                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Invalid state'
                ]);

                return $response;

            } else {

                // Try to get an access token (using the authorization code grant)
                $token = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code'],
                ]);

                // Optional: Now you have a token you can look up a users profile data

                // We got an access token, let's now get the owner details
                $ownerDetails = $provider->getResourceOwner($token);
                unset($_SESSION['oauth2state']);

                return $this->redirectToRoute('app_social', [
                    'id' => $ownerDetails->getId(),
                    'redSocial' => 'google',
                    'name' => $ownerDetails->getName(),
                    'email' => $ownerDetails->getEmail(),
                    'role' => 3
                ]);

            }

        } catch (Exception $e) {

            unset($_SESSION['oauth2state']);
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $e->getMessage()
            ]);

            return $response;

        }

    }

}
