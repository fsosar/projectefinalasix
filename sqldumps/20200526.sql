-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: server_management
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `id_activity` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `route` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request` text COLLATE utf8mb4_unicode_ci,
  `wildcard` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_activity`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
INSERT INTO `activity` VALUES (1,1,'login_attempt_fail',NULL,NULL,NULL,'en',1588606896),(2,1,'login_attempt_fail',NULL,NULL,NULL,'en',1589123303),(3,1,'login_attempt_fail',NULL,NULL,NULL,'en',1589123402),(4,1,'login_attempt_fail',NULL,NULL,NULL,'en',1589123416),(5,1,'login_attempt_fail',NULL,NULL,NULL,'en',1589123428),(6,1,'login_attempt_fail',NULL,NULL,NULL,'en',1590350875);
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blocked_stock`
--

DROP TABLE IF EXISTS `blocked_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blocked_stock` (
  `id_blocked_stock` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `time` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_blocked_stock`),
  KEY `fk_blocked_stock_id_member_idx` (`id_member`),
  KEY `fk_blocked_stock_id_product_idx` (`id_product`),
  CONSTRAINT `FK_FC77179C56D34F95` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`),
  CONSTRAINT `FK_FC77179CDD7ADDD` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blocked_stock`
--

LOCK TABLES `blocked_stock` WRITE;
/*!40000 ALTER TABLE `blocked_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocked_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `category_key` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cupons`
--

DROP TABLE IF EXISTS `cupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cupons` (
  `id_cupon` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `used` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_cupon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cupons`
--

LOCK TABLES `cupons` WRITE;
/*!40000 ALTER TABLE `cupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `cupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docker`
--

DROP TABLE IF EXISTS `docker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member` int(11) DEFAULT NULL,
  `id_docker` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `port` text,
  `docker_properties` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_docker_member_idx` (`member`),
  CONSTRAINT `fk_docker_member` FOREIGN KEY (`member`) REFERENCES `member` (`id_member`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docker`
--

LOCK TABLES `docker` WRITE;
/*!40000 ALTER TABLE `docker` DISABLE KEYS */;
INSERT INTO `docker` VALUES (1,1,'uzumymHpOk','asd','10000','1');
/*!40000 ALTER TABLE `docker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `command_line` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log`
--

LOCK TABLES `log` WRITE;
/*!40000 ALTER TABLE `log` DISABLE KEYS */;
/*!40000 ALTER TABLE `log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `id_member_role` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_surname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `docker_account` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` bigint(20) DEFAULT NULL,
  `modification_date` bigint(20) DEFAULT NULL,
  `deactivation_date` bigint(20) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_facebook` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_google` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` bigint(20) DEFAULT NULL,
  `last_activity` bigint(20) DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_validated` tinyint(1) DEFAULT NULL,
  `validated` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL,
  `email_secret_code` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_username` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_username_date` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_member`),
  KEY `fk_memberrole_usuario1_idx` (`id_member_role`),
  CONSTRAINT `FK_70E4FA78B3F36BC1` FOREIGN KEY (`id_member_role`) REFERENCES `member_role` (`id_Member_Role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,1,'alumne@alumne.com','Alumne',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606784,NULL,NULL,NULL,NULL,NULL,NULL,1590510769,'h',1,1,1,0,NULL,NULL,NULL),(2,1,'sebasti042@gmail.com','sebasti042',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606941,1588606941,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,0,'8551ed4bbd4c61ec4aec5672ea8bfb0529f7ea0252d5627adf6da5eb5d87baa90a40afac325a6324ba075c1397dd3046c6979218f4afe812cd1fe59bd1ad549f',NULL,NULL),(3,1,'psole@gmail.com','psole',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606941,1588606941,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,0,'8551ed4bbd4c61ec4aec5672ea8bfb0529f7ea0252d5627adf6da5eb5d87baa90a40afac325a6324ba075c1397dd3046c6979218f4afe812cd1fe59bd1ad549f',NULL,NULL);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_activity`
--

DROP TABLE IF EXISTS `member_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_activity` (
  `id_member_activity` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `key_activity` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha` bigint(20) NOT NULL,
  `details` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_member_activity`),
  KEY `id_Member` (`id_member`),
  CONSTRAINT `FK_5BA9AAB056D34F95` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_activity`
--

LOCK TABLES `member_activity` WRITE;
/*!40000 ALTER TABLE `member_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `member_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member_role`
--

DROP TABLE IF EXISTS `member_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_role` (
  `id_Member_Role` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `docker_max_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Member_Role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member_role`
--

LOCK TABLES `member_role` WRITE;
/*!40000 ALTER TABLE `member_role` DISABLE KEYS */;
INSERT INTO `member_role` VALUES (1,'ROLE_ADMINISTRADOR',10),(2,'ROLE_PROFESIONAL',5),(3,'ROLE_USUARIO',1);
/*!40000 ALTER TABLE `member_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20200504143007','2020-05-04 14:31:00');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id_product` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT NULL,
  `visible` tinyint(1) DEFAULT NULL,
  `category` text COLLATE utf8mb4_unicode_ci,
  `photos` text COLLATE utf8mb4_unicode_ci,
  `creation_date` bigint(20) DEFAULT NULL,
  `modification_date` bigint(20) DEFAULT NULL,
  `available_stock` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_product`),
  KEY `id_member` (`id_member`),
  CONSTRAINT `FK_D34A04AD56D34F95` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_cupon`
--

DROP TABLE IF EXISTS `product_cupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_cupon` (
  `id_product_cupon` int(11) NOT NULL AUTO_INCREMENT,
  `id_cupon` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_product_cupon`),
  KEY `fk_product_cupon_product_idx` (`id_product`),
  KEY `fk_product_cupon_cupon_idx` (`id_cupon`),
  CONSTRAINT `FK_F9938215B64AB879` FOREIGN KEY (`id_cupon`) REFERENCES `cupons` (`id_cupon`),
  CONSTRAINT `FK_F9938215DD7ADDD` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_cupon`
--

LOCK TABLES `product_cupon` WRITE;
/*!40000 ALTER TABLE `product_cupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_cupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_meta`
--

DROP TABLE IF EXISTS `product_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_meta` (
  `id_product_meta` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `locale` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_product_meta`),
  KEY `idproduct` (`id_product`),
  CONSTRAINT `FK_5A1A3694DD7ADDD` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_meta`
--

LOCK TABLES `product_meta` WRITE;
/*!40000 ALTER TABLE `product_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `id_member` int(11) DEFAULT NULL,
  `id_cupon` int(11) DEFAULT NULL,
  `facturation_number` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` bigint(20) DEFAULT NULL,
  `finalitzation_date` bigint(20) DEFAULT NULL,
  `details_member` text COLLATE utf8mb4_unicode_ci,
  `product` text COLLATE utf8mb4_unicode_ci,
  `total` int(11) DEFAULT NULL,
  `finished` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`),
  KEY `fk_billing_id_Member_idx` (`id_member`),
  KEY `fk_transaction_id_cupon_idx` (`id_cupon`),
  CONSTRAINT `FK_723705D156D34F95` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`),
  CONSTRAINT `FK_723705D1B64AB879` FOREIGN KEY (`id_cupon`) REFERENCES `cupons` (`id_cupon`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'server_management'
--

--
-- Dumping routines for database 'server_management'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-26 18:45:51
