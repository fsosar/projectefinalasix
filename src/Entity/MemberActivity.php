<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MemberActivity
 *
 * @ORM\Table(name="member_activity", indexes={@ORM\Index(name="id_Member", columns={"id_member"})})
 * @ORM\Entity
 */
class MemberActivity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_member_activity", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMemberActivity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="key_activity", type="string", length=200, nullable=true)
     */
    private $keyActivity;

    /**
     * @var int
     *
     * @ORM\Column(name="fecha", type="bigint", nullable=false)
     */
    private $fecha;

    /**
     * @var string|null
     *
     * @ORM\Column(name="details", type="string", length=200, nullable=true)
     */
    private $details;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=true)
     */
    private $id;

    /**
     * @var \Member
     *
     * @ORM\ManyToOne(targetEntity="Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_member", referencedColumnName="id_member")
     * })
     */
    private $idMember;

    public function getIdMemberActivity(): ?int
    {
        return $this->idMemberActivity;
    }

    public function getKeyActivity(): ?string
    {
        return $this->keyActivity;
    }

    public function setKeyActivity(?string $keyActivity): self
    {
        $this->keyActivity = $keyActivity;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(?string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIdMember(): ?Member
    {
        return $this->idMember;
    }

    public function setIdMember(?Member $idMember): self
    {
        $this->idMember = $idMember;

        return $this;
    }


}
