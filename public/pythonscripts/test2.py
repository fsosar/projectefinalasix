#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys    # per importar la llibreria necessària
import datetime
import subprocess
args = sys.argv
import os
del args[0]

docker={}

for elem in args:
	if elem.startswith("--"):
		elem = elem.replace("--","")
		elem = elem.split("=")
		argex = elem[0]
		arval = elem[1]
		docker[argex] = arval
print(docker)

plantilla1="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'tomcat:latest'\n        ports:\n            - '"+ docker["port"]+":8080'\n        expose:\n            - '8080'"
plantilla2="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'mysql/mysql-server:latest'\n        ports:\n           - '"+ docker["port"]+":3306'\n        expose:\n            - '3306'"
plantilla3="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'firespring/apache2-php:latest'\n        ports:\n            - '"+ docker["port"]+":80'\n        expose:\n            - '80'"
plantilla4="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'mattrayner/lamp:latest'\n        ports:\n            - '"+ docker["port"]+":80'\n        expose:\n            - '80'"
plantilla5="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'mediawiki:latest'\n        ports:\n            - '"+ docker["port"]+":80'\n        expose:\n            - '80'"
plantilla6="version: '3'\nservices:\n    " +docker["idname"]+ ":\n        image: 'pgadmin4:latest'\n        ports:\n            - '"+ docker["port"]+":5050'\n        expose:\n            - '5050'"

dockerstart = "/home/projecte/dockervol/"+docker["idname"]+".yml"

if(docker["dockername"]=="tomcat"):
	dockercreate = open(dockerstart,'a')
	dockercreate.write(plantilla1)
	dockercreate.close()
	dockercreate = open(dockerstart,'r')
	prueba=dockercreate.read()
	print(prueba)
	proc = os.system("docker stack deploy --compose-file "+dockerstart+" "+docker["idname"])
	print(str(proc))
elif(docker["dockername"]=="mysql"):
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'a')
	dockercreate.write(plantilla2)
	dockercreate.close()
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'r')
	prueba=dockercreate.read()
	print(prueba)
elif(docker["dockername"]=="apache"):
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'a')
	dockercreate.write(plantilla3)
	dockercreate.close()
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'r')
	prueba=dockercreate.read()
	print(prueba)
elif(docker["dockername"]=="lamp"):
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'a')
	dockercreate.write(plantilla4)
	dockercreate.close()
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'r')
	prueba=dockercreate.read()
	print(prueba)
elif(docker["dockername"]=="mediawiki"):
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'a')
	dockercreate.write(plantilla1)
	dockercreate.close()
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'r')
	prueba=dockercreate.read()
	print(prueba)
elif(docker["dockername"]=="pgadmin2"):
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'a')
	dockercreate.write(plantilla1)
	dockercreate.close()
	dockercreate = open("/home/projecte/dockervol/"+ docker["idname"]+".yml",'r')
	prueba=dockercreate.read()
	print(prueba)

#proc = os.system("docker stack deploy --compose-file ../dockeryml/docker-compose.yml mysql")
#proc = os.system("ls -l")
#print(str(proc))
#print("hola, este es un script de python")
