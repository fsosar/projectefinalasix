<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Docker
 *
 * @ORM\Table(name="docker", indexes={@ORM\Index(name="fk_docker_member_idx", columns={"member"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\DockerRepository")
 */
class Docker
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="id_docker", type="string", length=50, nullable=true)
     */
    private $idDocker;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="port", type="text", length=65535, nullable=true)
     */
    private $port;

    /**
     * @var string|null
     *
     * @ORM\Column(name="docker_properties", type="string", length=50, nullable=true)
     */
    private $dockerProperties;

    /**
     * @var \Member
     *
     * @ORM\ManyToOne(targetEntity="Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="member", referencedColumnName="id_member")
     * })
     */
    private $member;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdDocker(): ?string
    {
        return $this->idDocker;
    }

    public function setIdDocker(?string $idDocker): self
    {
        $this->idDocker = $idDocker;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPort(): ?string
    {
        return $this->port;
    }

    public function setPort(?string $port): self
    {
        $this->port = $port;

        return $this;
    }

    public function getDockerProperties(): ?string
    {
        return $this->dockerProperties;
    }

    public function setDockerProperties(?string $dockerProperties): self
    {
        $this->dockerProperties = $dockerProperties;

        return $this;
    }

    public function getMember(): ?Member
    {
        return $this->member;
    }

    public function setMember(?Member $member): self
    {
        $this->member = $member;

        return $this;
    }


}
