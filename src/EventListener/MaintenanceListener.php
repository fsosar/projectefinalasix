<?php
namespace App\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class MaintenanceListener
{
    private $container, $maintenance, $isAuthorized, $security;

    public function __construct($maintenance, $security, ContainerInterface $container)
    {
        $this->container = $container;
        $this->maintenance = $maintenance['status'];
        $this->isAuthorized = $maintenance['isAuthorized'];
        $this->security = $security;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        // This will get the value of our maintenance parameter
        $maintenance = $this->maintenance ? $this->maintenance : false;
        $currentIP = $_SERVER['REMOTE_ADDR'];
        $response = new JsonResponse();
        // This will detect if we are in dev environment (app_dev.php)
        // $debug = in_array($this->container->get('kernel')->getEnvironment(), ['dev']);
        
        $route = 'app_login';
        //$route = $event->getRequest()->attributes->all()['_route'];

        // If maintenance is active and in prod environment
        if ($maintenance and !in_array($currentIP, $this->isAuthorized) and !$this->security->isGranted('ROLE_ADMINISTRADOR') && $route != 'app_login') {
            // We load our maintenance template
            $engine = $this->container->get('templating');
            $template = $engine->render('maintenance/maintenance.html.twig');
            // We send our response with a 503 response code (service unavailable)
            $event->setResponse(new Response($template, 503));
            $event->stopPropagation();
        }
    }
}
