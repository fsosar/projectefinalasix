<?php
namespace App\Services;

use App\Entity\Product;
use App\Entity\Category;

class ServicesProduct
{
    public function searchProduct($valores)
    {
        $em = $valores['em'];
        $localeProduct = $valores['localeProduct'];
        $idProduct = $valores['idProduct'];
        $filtreMinPrice = $valores['filtreMinPrice'];
        $filtreMaxPrice = $valores['filtreMaxPrice'];
        $filtreCategory = $valores['filtreCategory'];

        $product = $em->getRepository(Product::class)->searchProduct($valores);
        $category = $em->getRepository(Category::class)->findAll();

        // Convertir el Json de categoria en array con todos los datos de la categoria
        for ($i = 0; $i < count($product); $i++) {
            $product[$i]['category'] = json_decode($product[$i]['category'], true);
            for ($a = 0; $a < count($category); $a++) {
                if ($product[$i]['category']) {
                    for ($b = 0; $b < count($product[$i]['category']); $b++) {
                        if ($category[$a]->getIdCategory() == $product[$i]['category'][$b]) {
                            $product[$i]['category'][$b] = array(
                                'idCategory' => $category[$a]->getIdCategory(),
                                'categoryKey' => $category[$a]->getCategoryKey(),
                                'name' => $category[$a]->getName(),
                                'father' => $category[$a]->getFather(),
                                'active' => $category[$a]->getActive(),
                            );
                        }
                    }
                }
            }
            // FOTOS DE JSON A ARRAY
            $product[$i]['photos'] = json_decode($product[$i]['photos'], true);
        }

        // FOLTROS
        // Category
        $temp = array();
        if ($filtreCategory) {
            for ($j = 0; $j < count($product); $j++) {
                for ($c = 0; $c < count($filtreCategory); $c++) {
                    if (is_array($product[$j]['category'])) {
                        for ($d = 0; $d < count($product[$j]['category']); $d++) {
                            if ($filtreCategory[$c] == $product[$j]['category'][$d]['idCategory']) {
                                array_push($temp, $product[$j]);
                            }
                        }
                    }
                }
            }
            $product = $temp;
        }
        return $product;
    }
}
