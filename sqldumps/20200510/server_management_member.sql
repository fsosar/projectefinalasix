CREATE DATABASE  IF NOT EXISTS `server_management` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `server_management`;
-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: server_management
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `id_member_role` int(11) DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `second_surname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `docker_account` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creation_date` bigint(20) DEFAULT NULL,
  `modification_date` bigint(20) DEFAULT NULL,
  `deactivation_date` bigint(20) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_facebook` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key_google` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` bigint(20) DEFAULT NULL,
  `last_activity` bigint(20) DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_validated` tinyint(1) DEFAULT NULL,
  `validated` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL,
  `email_secret_code` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_username` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `change_username_date` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_member`),
  KEY `fk_memberrole_usuario1_idx` (`id_member_role`),
  CONSTRAINT `FK_70E4FA78B3F36BC1` FOREIGN KEY (`id_member_role`) REFERENCES `member_role` (`id_Member_Role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,1,'alumne@alumne.com','Alumne',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606784,NULL,NULL,NULL,NULL,NULL,NULL,1589124190,'h',1,1,1,0,NULL,NULL,NULL),(2,1,'sebasti042@gmail.com','sebasti042',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606941,1588606941,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,0,'8551ed4bbd4c61ec4aec5672ea8bfb0529f7ea0252d5627adf6da5eb5d87baa90a40afac325a6324ba075c1397dd3046c6979218f4afe812cd1fe59bd1ad549f',NULL,NULL),(3,1,'psole@gmail.com','psole',NULL,NULL,'$argon2i$v=19$m=65536,t=4,p=1$eW1SamxSLngxOUtOakp5ZQ$ATggXNRfXGVA720LdwWGR2L6sjVO09fbXFGD3+vcKNI',NULL,1588606941,1588606941,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1,1,0,'8551ed4bbd4c61ec4aec5672ea8bfb0529f7ea0252d5627adf6da5eb5d87baa90a40afac325a6324ba075c1397dd3046c6979218f4afe812cd1fe59bd1ad549f',NULL,NULL);
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-10 17:25:45
