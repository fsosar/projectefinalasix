<?php

namespace App\Controller\Transaction;

use App\Entity\BlockedStock;
use App\Entity\Cupons;
use App\Entity\Product;
use App\Entity\ProductCupon;
use App\Entity\Member;
use App\Entity\Transaction;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TransactionController extends Controller
{
    /**
     * @Route("/{_locale}/transaction/add-to-cart")
     */
    public function addToCartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $response = new JsonResponse();

        $idProduct = $request->request->get('idProduct');
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');

        $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);

        if (!$product) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal, por favor intentalo mas tarde'
            ]);

            return $response;
        }

        $member = $idMember;
        $idProduct = $product->getIdProduct();
        $productPrice = $product->getPrice();

        $productAdd = [
            'idProduct' => $idProduct,
            'quantity' => 1,
            'totalPrice' => $productPrice,
            'product' => $product
        ];

        if (!$loggedIn) {

            $session = $request->getSession();

            if ($session->get('cart')) {

                $products = json_decode($session->get('cart'), true);

                $exists = false;

                foreach ($products as $key => $product) {

                    if (in_array($productAdd['idProduct'], $product)) {

                        $products[$key]['quantity'] = $products[$key]['quantity'] + 1;
                        $products[$key]['totalPrice'] = $products[$key]['totalPrice'] + $productAdd['totalPrice'];

                        $exists = true;
                    }
                }

                if (!$exists) {
                    array_push($products, $productAdd);
                }

                $session->set('cart', $serializer->serialize($products, 'json'));
            } else {
                $newProduct = [];
                array_push($newProduct, $productAdd);
                $session->set('cart', $serializer->serialize($newProduct, 'json'));
            }

            $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);
            $idBlockedStock = "";

            if ($session->get('blockStock')) {

                $idBlockedStock = json_decode($session->get('blockStock'), true);
                $blockedStock = $em->getRepository(BlockedStock::class)->findBy(['idBlockedStock' => $idBlockedStock]);
                $exists = false;
                foreach ($blockedStock as $block) {

                    if ($block->getIdProduct()->getIdProduct() == $idProduct) {

                        $block->setQuantity($block->getQuantity() + 1);
                        $block->setTime(time());
                        $em->persist($block);
                        $em->flush();
                        $exists = true;
                    }
                }

                if (!$exists) {

                    $blockedStock = new BlockedStock();

                    $blockedStock->setIdProduct($product);
                    $blockedStock->setIdMember(null);
                    $blockedStock->setQuantity(1);
                    $blockedStock->setTime(time());

                    $em->persist($blockedStock);
                    $em->flush();

                    array_push($idBlockedStock, $blockedStock->getIdBlockedStock());

                    $session->set('blockStock', $serializer->serialize($idBlockedStock, 'json'));
                }
            } else {

                $blockedStock = new BlockedStock();

                $blockedStock->setIdProduct($product);
                $blockedStock->setIdMember(null);
                $blockedStock->setQuantity(1);
                $blockedStock->setTime(time());

                $em->persist($blockedStock);
                $em->flush();

                $stocks = [$blockedStock->getIdBlockedStock()];

                $session->set('blockStock', $serializer->serialize($stocks, 'json'));
            }

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success',
                'msg' => 'Añadido el producto correctamente a el carro.'
            ]);
        } else {

            $idMember = $this->get('security.token_storage')->getToken()->getUser()->getIdMember();

            $total = 0;
            // END Buscar el producto

            // Buscamos el carrito
            $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ["idTransaction" => 'DESC']);
            // END Buscar el carrito

            // Buscamos el Member
            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember, 'deleted' => 0]);

            $detailsMember = [
                'name' => $member->getName(),
                'username' => $member->getUsername(),
                'apellido' => $member->getSurname()
            ];
            $detailsMember = $serializer->serialize($detailsMember, 'json');
            // END Buscamos el Member

            // try para ver si el carrito ya existe o no.
            try {

                if (!$transaction) {
                    $transaction = new Transaction();
                }

                $transaction->setIdMember($member);
                $transaction->setCreationDate(time());
                $transaction->setDetailsMember($detailsMember);

                if ($transaction->getProduct()) {

                    $products = json_decode($transaction->getProduct(), true);

                    $exists = false;

                    foreach ($products as $key => $product) {

                        if ($productAdd['idProduct'] == $product['idProduct']) {

                            $products[$key]['quantity'] = $products[$key]['quantity'] + 1;
                            $products[$key]['totalPrice'] = $products[$key]['totalPrice'] + $productAdd['totalPrice'];
                            $total = $total + $products[$key]['totalPrice'];

                            $exists = true;
                        }
                    }

                    if (!$exists) {
                        $total = $transaction->getTotal() + $productAdd['totalPrice'];
                        array_push($products, $productAdd);
                    }

                    $transaction->setProduct($serializer->serialize($products, 'json'));
                } else {

                    $total = $total + $productAdd['totalPrice'];
                    $newProduct = [];
                    array_push($newProduct, $productAdd);
                    $transaction->setProduct($serializer->serialize($newProduct, 'json'));
                }

                $transaction->setTotal($total);

                $transaction->setFinished(0);

                $em->persist($transaction);
                $em->flush();

                $response->setStatusCode(200);
                $response->setData([
                    'response' => 'success',
                    'msg' => 'Anadido el producto correctamente a el carro.'
                ]);
            } catch (JsonException $e) {
                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'No se pudo añadir el producto a el carro.'
                ]);
            }

            $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);
            $blockedStock = $em->getRepository(BlockedStock::class)->findOneBy(['idProduct' => $product, 'idMember' => $member]);

            if ($blockedStock) {

                $blockedStock->setQuantity($blockedStock->getQuantity() + 1);
                $blockedStock->setTime(time());
            } else {

                $blockedStock = new BlockedStock();

                $blockedStock->setIdProduct($product);
                $blockedStock->setIdMember($member);
                $blockedStock->setQuantity(1);
                $blockedStock->setTime(time());
            }

            $product->setAvailableStock($blockedStock->getQuantity());

            $em->persist($blockedStock);
            $em->persist($product);
            $em->flush();
        }

        return $response;
    }

    /**
     * @Route("/{_locale}/transaction/show-cart")
     */
    public function showCartAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $transactions = [];

        if ($loggedIn) {

            // Buscar si existe el carrito
            $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ["creationDate" => "DESC"]);

            // END Buscar si existe el carrito
            if ($transaction) {
                $transactions = json_decode($transaction->getProduct(), true);
            }
        } else {

            $session = $request->getSession();

            $transactions = json_decode($session->get('cart'), true);
        }

        return $this->render('transaction/cart.html.twig', [
            "transaction" => $transactions
        ]);
    }

    /**
     * @Route("/{_locale}/transaction/cart")
     */
    public function showFinishAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');

        if (!$loggedIn) {
            return $this->redirectToRoute('app_login');
        }
        
        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ['creationDate' => 'DESC']);
        $productsInCart = json_decode($transaction->getProduct(),true);
        $discountedTotal = $transaction->getTotal();
        
        if($transaction && $transaction->getIdCupon()){

            $cupon = $transaction->getIdCupon();

            $products = $em->getRepository(ProductCupon::class)->findBy(array('idCupon' => $cupon));
        
            $decodedTransactions = json_decode($transaction->getProduct(), true);
            $productsPrice = 0;
            $discountedTotal = 0;
        
            foreach ($decodedTransactions as $trans) {
        
                $productsPrice = $trans['totalPrice'];
        
                foreach ($products as $product) {
        
                    if ($product->getIdProduct()->getIdProduct() == $trans['idProduct']) {
        
                        if ($cupon->getType() == "%") {
        
                            $productsPrice = ($trans['totalPrice'] - ($cupon->getQuantity() / 100 * $trans['totalPrice']));
        
                        } else if ($cupon->getType() == "€") {
        
                            $productsPrice = $trans['totalPrice'] - $cupon->getQuantity();
        
                        }
        
                    }
        
                }
        
        
                $discountedTotal = $discountedTotal + $productsPrice;
        
            }
        }

        return $this->render('transaction/finish.html.twig', [
            'transaction' => $transaction,
            'productsInCart' => $productsInCart,
            'price' => $discountedTotal
        ]);
    }

    /**
     * @Route("/{_locale}/transaction/pay/{idTransaction}")
     */
    public function showCheckoutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idTransaction = $request->get('idTransaction');

        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idTransaction' => $idTransaction, 'finished' => 0], ['creationDate' => 'DESC']);

        if (!$transaction) {
            return $this->redirectToRoute('homepage');
        }

        return $this->render('transaction/checkout.html.twig', [
            'transaction' => $transaction
        ]);
    }

    /**
     * @Route("/transaction/finish")
     */
    public function checkOutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $response = new JsonResponse();

        $idTransaction = $request->request->get('idTransaction');

        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idTransaction' => $idTransaction, 'finished' => 0, 'idMember' => $idMember], ['creationDate' => 'DESC']);

        $idProducts = [];
        $products = json_decode($transaction->getProduct(), true);
        foreach ($products as $product) {
            array_push($idProducts, $product['idProduct']);
        }

        $products = $em->getRepository(Product::class)->findBy(['idProduct' => $idProducts]);
        $blockedStock = $em->getRepository(BlockedStock::class)->findBy(['idProduct' => $products, 'idMember' => $idMember]);

        foreach ($blockedStock as $block) {
            $em->remove($block);
        }

        if (!$transaction) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'error'
            ]);
            return $response;
        }

        $numeroFactura = $helper->getNumeroFactura($idMember);

        $transaction->setFinished(1);
        $transaction->setFacturationNumber($numeroFactura);
        $productsInCart = json_decode($transaction->getProduct(),true);

        $html = $this->renderView('templates/transactions/invoice/invoice.html.twig', [
            'member' => $idMember,
            'transaction' => $transaction,
            'productsInCart' => $productsInCart,
            'products' => $products,
            'numeroFactura' => $numeroFactura
        ]);

        $member = $idMember;
        $idMember = $idMember->getIdMember();
        $path = 'member/$idMember/pdf/';

        if (!file_exists('member/')) {
            mkdir('member/');
        }

        $newPath = 'member/' . $idMember;
        if (!file_exists($newPath)) {

            mkdir($newPath);
        }

        $newPath = 'member/' . $idMember . "/pdf/";
        if (!file_exists($newPath)) {

            mkdir($newPath);
        }

        $newPath = 'member/' . $idMember . "/pdf/" . time();
        if (!file_exists($newPath)) {
            mkdir($newPath);
        }

        $filenameReal = $numeroFactura;

        $rutaReal = 'member/' . $idMember . '/pdf/' . time() . '/' . $filenameReal . '.pdf';
        $invoiceRoute = 'member/' . $idMember . '/pdf/' . time() . '/' . $filenameReal . '.pdf';

        if (file_exists($rutaReal)) {

            unlink($rutaReal);
        }

        $this->get('knp_snappy.pdf')->generateFromHtml($html, $rutaReal, []);

        $em->persist($transaction);
        $em->flush();

        $manager_mail->templeteOne([
            'to' => $member->getUsername(),
            'subject' => 'Factura de producto comprado',
            'body' =>
            $this->renderView('templates/emails/transaction/mail_invoice.html.twig', [
                'member' => $member,
                'products' => $products
            ])
        ]);

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Done'
        ]);

        return $response;
    }

    /**
     * @Route("/transaction/delete-from-cart")
     */
    public function deleteFromCartAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $response = new JsonResponse();
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $idProduct = $request->request->get('idProduct');

        if ($loggedIn) {

            $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ['creationDate' => 'DESC']);

            if (!$transaction) {

                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Algo ha ido mal, vuelve a intentarlo mas tarde',
                ]);

                return $response;
            }

            $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);

            if (!$product) {

                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Algo ha ido mal, vuelve a intentarlo mas tarde'
                ]);

                return $response;
            }

            $products = json_decode($transaction->getProduct(), true);
            $total = $transaction->getTotal();

            foreach ($products as $key => $product) {

                if ($product['idProduct'] == $idProduct) {

                    $total = $total - $product['totalPrice'];

                    unset($products[$key]);
                }
            }

            $products = $serializer->serialize($products, 'json');

            $transaction->setProduct($products);
            $transaction->setTotal($total);

            $blockedStock = $em->getRepository(BlockedStock::class)->findOneBy(['idProduct' => $product, 'idMember' => $idMember]);
            $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);

            if ($blockedStock) {

                $product->setAvailableStock($product->getAvailableStock() - $blockedStock->getQuantity());
                $em->remove($blockedStock);

                $em->persist($product);
            }

            $em->persist($transaction);
            $em->flush();

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success'
            ]);

            return $response;
        } else {

            $session = $request->getSession();

            $product = $em->getRepository(Product::class)->findOneBy(['idProduct' => $idProduct]);

            if (!$product) {

                $response->setStatusCode(500);
                $response->setData([
                    'response' => 'error',
                    'msg' => 'Algo ha ido mal, vuelve a intentarlo mas tarde'
                ]);

                return $response;
            }

            if ($session->get('cart')) {

                $products = json_decode($session->get('cart'), true);

                foreach ($products as $key => $product) {

                    if ($product['idProduct'] == $idProduct) {

                        unset($products[$key]);
                    }
                }

                $session->set('cart', $serializer->serialize($products, 'json'));
            }

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success'
            ]);

            return $response;
        }
    }

    /**
     * @Route("{_locale}/transaction/cupons")
     */
    public function showCuponsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $idMember = $this->get('security.token_storage')->getToken()->getUser();

        $products = $em->getRepository(Product::class)->findAll();

        return $this->render('backend/transaction/cupons/html.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("{_locale}/transaction/cupons/search")
     */
    public function buscadorCuponsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $idMember = $this->get('security.token_storage')->getToken()->getUser();

        $cupons = $em->getRepository(Cupons::class)->findAll();
        $products = $em->getRepository(ProductCupon::class)->findAll();
        // $products = $em->getRepository(Product::class)->findAll();

        return $this->render('backend/transaction/cupons/table.html.twig', [
            'cupons' => $cupons,
            'products' => $products
        ]);
    }

    /**
     * @Route("{_locale}/transaction/coupons/add")
     */
    public function addCouponsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $response = new JsonResponse();

        $quantity = $request->request->get('quantity');
        $type = $request->request->get('type');
        $idProduct = $request->request->get('products');

        $products = $em->getRepository(Product::class)->findBy(['idProduct' => $idProduct]);

        if (!$idProduct || !$products) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }

        if (!$quantity) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'La cantidad no puede estar vacia'
            ]);

            return $response;
        }

        if (!$type) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Por favor selecciona un tipo de descuento'
            ]);

            return $response;
        } elseif ($type != "%" && $type != "€") {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }

        try {
            $coupons = $em->getRepository(Cupons::class)->findOneBy(['quantity' => $quantity, 'type' => $type]);

            if (!$coupons) {

                $coupons = new Cupons();

                $coupons->setQuantity($quantity);
                $coupons->setType($type);
                $coupons->setUsed(0);

                $em->persist($coupons);
            }

            foreach ($products as $product) {

                $productCupon = new ProductCupon();

                $productCupon->setIdCupon($coupons);
                $productCupon->setIdProduct($product);

                $em->persist($productCupon);
            }

            $em->flush();

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success'
            ]);

            return $response;
        } catch (Exception $e) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }
    }

    /**
     * @Route("/transaction/coupon/apply")
     */
    public function applyCouponAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $translator = $this->get('translator');
        $response = new JsonResponse();

        $promotionalCode = $request->request->get('promotionalCode');

        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ['creationDate' => 'DESC']);

        if (!$transaction) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }

        if ($transaction->getIdCupon()) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Cupon ya usado'
            ]);

            return $response;
        }

        $coupon = $em->getRepository(Cupons::class)->findOneBy(['idCupon' => $promotionalCode]);

        if (!$coupon) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Este cupon no existe'
            ]);

            return $response;
        }

        $transaction->setIdCupon($coupon);

        $em->persist($transaction);
        $em->flush();

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'type' => $coupon->getType(),
            'quantity' => $coupon->getQuantity()
        ]);

        return $response;
    }

    /**
     * @Route("{_locale}/transaction/coupon/delete")
     */
    public function deleteCouponAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $response = new JsonResponse();

        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ['creationDate' => 'DESC']);

        if (!$transaction) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }

        try {

            $transaction->setIdCupon(null);

            $em->persist($transaction);
            $em->flush();
        } catch (Exception $e) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Algo ha ido mal'
            ]);

            return $response;
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success'
        ]);

        return $response;
    }

    /**
     * @Route("{_locale}/transaction/update-quantity")
     */
    public function updateQuantityAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $idMember = $this->get('security.token_storage')->getToken()->getUser();
        $translator = $this->get('translator');
        $response = new JsonResponse();
        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);

        $idProduct = $request->request->get('idProduct');
        $quantity = $request->request->get('quantity');

        $transaction = $em->getRepository(Transaction::class)->findOneBy(['idMember' => $idMember, 'finished' => 0], ['creationDate' => 'DESC']);

        if (!$transaction) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $translator->trans('Algo ha ido mal')
            ]);

            return $response;
        }

        $products = json_decode($transaction->getProduct(), true);
        $totalPrice = 0;

        foreach ($products as $key => $product) {

            if ($idProduct == $product['idProduct']) {

                $products[$key]['quantity'] = $quantity;
                $products[$key]['totalPrice'] = $products[$key]['product']['price'] * $quantity;
            }

            $totalPrice = $totalPrice + $products[$key]['totalPrice'];
        }

        $transaction->setProduct($serializer->serialize($products, 'json'));
        $transaction->setTotal($totalPrice);

        $em->persist($transaction);
        $em->flush();

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success'
        ]);

        return $response;
    }

    /**
     * @Route("{_locale}/dashboard/transactions",defaults={"username":null})
     */
    public function showTransactionsUserAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $username = $request->get('username');

        if (!$loggedIn) {
            return $this->redirectToRoute('app_login');
        }

        if (!$username) {

            $username = "";
        }

        $transactions = $em->getRepository(Transaction::class)->findBy(['finished' => 1]);

        $idTransactions = [];
        foreach ($transactions as $transaction) {

            array_push($idTransactions, $transaction->getIdTransaction());
        }

        return $this->render('backend/transaction/html.html.twig', [
            'username' => $username,
            'transactions' => $transactions,
            'idTransactions' => $idTransactions
        ]);
    }

    /**
     * @Route("{_locale}/dashboard/transaction/excel")
     */
    public function generateCsvAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $response = new JsonResponse();

        $idTransactions = $request->request->get('idTransactions');

        $idTransactions = explode(",", $idTransactions);

        $transactions = $em->getRepository(Transaction::class)->findBy(['idTransaction' => $idTransactions]);

        if (!$transactions) {

            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $translator->trans('ERRORS.unexpectedError')
            ]);

            return $response;
        }

        $products = [];
        $row = 1;
        $exists = false;

        foreach ($transactions as $countTransaction) {

            foreach (json_decode($countTransaction->getProduct(), true) as $countProduct) {
            }
        }

        $arrayData = [
            ['Factura', 'Nombre', 'Apellidos', 'Username', 'NIF', 'Nombre product', 'Cantidad', 'Precio unidad', 'Precio total'],
        ];

        foreach ($transactions as $transaction) {

            $exists = false;
            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $transaction->getIdMember()]);

            foreach (json_decode($transaction->getProduct(), true) as $product) {

                $productToInsert = $em->getRepository(Product::class)->findOneBy(['idProduct' => $product['idProduct']]);
                if (!$exists) {
                    $products['facturationNumber' . $row] = $transaction->getFacturationNumber();
                    $products['memberName' . $row] = $member->getName();
                    $products['surname' . $row] = $member->getSurname() . " " . $member->getSecondSurname();
                    $products['username' . $row] = $member->getUsername();
                    $exists = true;
                }

                $products['name' . $row] = $productToInsert->getIdProduct();
                $products['quantity' . $row] = $product['quantity'];
                $products['unitPrice' . $row] = $productToInsert->getPrice();
                $products['totalPrice' . $row] = $product['totalPrice'];
                $row++;
            }

            array_push($arrayData, $products);
            $products = [];
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $writer = new Csv($spreadsheet);

        $spreadsheet->getActiveSheet()->fromArray(
            $arrayData,
            null,
            'A1'
        );

        $writer->save('transactions.csv');

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Generado'
        ]);

        return $response;
    }

    /**
     * @Route("{_locale}/download/csv")
     */
    public function downloadExcelAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $file = new File('transactions.csv');

        return $this->file($file);
    }
}
