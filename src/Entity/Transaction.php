<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction", indexes={@ORM\Index(name="fk_billing_id_Member_idx", columns={"id_member"}), @ORM\Index(name="fk_transaction_id_cupon_idx", columns={"id_cupon"})})
 * @ORM\Entity
 */
class Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_transaction", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idTransaction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="facturation_number", type="string", length=50, nullable=true)
     */
    private $facturationNumber;

    /**
     * @var int|null
     *
     * @ORM\Column(name="creation_date", type="bigint", nullable=true)
     */
    private $creationDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="finalitzation_date", type="bigint", nullable=true)
     */
    private $finalitzationDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="details_member", type="text", length=65535, nullable=true)
     */
    private $detailsMember;

    /**
     * @var string|null
     *
     * @ORM\Column(name="product", type="text", length=65535, nullable=true)
     */
    private $product;

    /**
     * @var int|null
     *
     * @ORM\Column(name="total", type="integer", nullable=true)
     */
    private $total;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="finished", type="boolean", nullable=true)
     */
    private $finished;

    /**
     * @var \Member
     *
     * @ORM\ManyToOne(targetEntity="Member")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_member", referencedColumnName="id_member")
     * })
     */
    private $idMember;

    /**
     * @var \Cupons
     *
     * @ORM\ManyToOne(targetEntity="Cupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cupon", referencedColumnName="id_cupon")
     * })
     */
    private $idCupon;

    public function getIdTransaction(): ?int
    {
        return $this->idTransaction;
    }

    public function getFacturationNumber(): ?string
    {
        return $this->facturationNumber;
    }

    public function setFacturationNumber(?string $facturationNumber): self
    {
        $this->facturationNumber = $facturationNumber;

        return $this;
    }

    public function getCreationDate(): ?string
    {
        return $this->creationDate;
    }

    public function setCreationDate(?string $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getFinalitzationDate(): ?string
    {
        return $this->finalitzationDate;
    }

    public function setFinalitzationDate(?string $finalitzationDate): self
    {
        $this->finalitzationDate = $finalitzationDate;

        return $this;
    }

    public function getDetailsMember(): ?string
    {
        return $this->detailsMember;
    }

    public function setDetailsMember(?string $detailsMember): self
    {
        $this->detailsMember = $detailsMember;

        return $this;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function setProduct(?string $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getTotal(): ?int
    {
        return $this->total;
    }

    public function setTotal(?int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getFinished(): ?bool
    {
        return $this->finished;
    }

    public function setFinished(?bool $finished): self
    {
        $this->finished = $finished;

        return $this;
    }

    public function getIdMember(): ?Member
    {
        return $this->idMember;
    }

    public function setIdMember(?Member $idMember): self
    {
        $this->idMember = $idMember;

        return $this;
    }

    public function getIdCupon(): ?Cupons
    {
        return $this->idCupon;
    }

    public function setIdCupon(?Cupons $idCupon): self
    {
        $this->idCupon = $idCupon;

        return $this;
    }


}
