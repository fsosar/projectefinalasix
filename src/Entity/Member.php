<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Member
 *
 * @ORM\Table(name="member", indexes={@ORM\Index(name="fk_memberrole_usuario1_idx", columns={"id_member_role"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 */
class Member implements AdvancedUserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_member", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMember;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=100, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=100, nullable=true)
     */
    private $surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="second_surname", type="string", length=100, nullable=true)
     */
    private $secondSurname;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="docker_account", type="string", length=50, nullable=true)
     */
    private $dockerAccount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="creation_date", type="bigint", nullable=true)
     */
    private $creationDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="modification_date", type="bigint", nullable=true)
     */
    private $modificationDate;

    /**
     * @var int|null
     *
     * @ORM\Column(name="deactivation_date", type="bigint", nullable=true)
     */
    private $deactivationDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var string|null
     *
     * @ORM\Column(name="key_facebook", type="string", length=100, nullable=true)
     */
    private $keyFacebook;

    /**
     * @var string|null
     *
     * @ORM\Column(name="key_google", type="string", length=100, nullable=true)
     */
    private $keyGoogle;

    /**
     * @var int|null
     *
     * @ORM\Column(name="birthday", type="bigint", nullable=true)
     */
    private $birthday;

    /**
     * @var int|null
     *
     * @ORM\Column(name="last_activity", type="bigint", nullable=true)
     */
    private $lastActivity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="gender", type="string", length=1, nullable=true)
     */
    private $gender;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="email_validated", type="boolean", nullable=true)
     */
    private $emailValidated;

    /**
     * @var bool
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false, options={"default"="1"})
     */
    private $isActive = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email_secret_code", type="string", length=500, nullable=true)
     */
    private $emailSecretCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="change_username", type="string", length=45, nullable=true)
     */
    private $changeUsername;

    /**
     * @var int|null
     *
     * @ORM\Column(name="change_username_date", type="bigint", nullable=true)
     */
    private $changeUsernameDate;

    /**
     * @var \MemberRole
     *
     * @ORM\ManyToOne(targetEntity="MemberRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_member_role", referencedColumnName="id_Member_Role")
     * })
     */
    private $idMemberRole;

    public function getIdMember(): ?int
    {
        return $this->idMember;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getSecondSurname(): ?string
    {
        return $this->secondSurname;
    }

    public function setSecondSurname(?string $secondSurname): self
    {
        $this->secondSurname = $secondSurname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getDockerAccount(): ?string
    {
        return $this->dockerAccount;
    }

    public function setDockerAccount(?string $dockerAccount): self
    {
        $this->dockerAccount = $dockerAccount;

        return $this;
    }

    public function getCreationDate(): ?string
    {
        return $this->creationDate;
    }

    public function setCreationDate(?string $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getModificationDate(): ?string
    {
        return $this->modificationDate;
    }

    public function setModificationDate(?string $modificationDate): self
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    public function getDeactivationDate(): ?string
    {
        return $this->deactivationDate;
    }

    public function setDeactivationDate(?string $deactivationDate): self
    {
        $this->deactivationDate = $deactivationDate;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getKeyFacebook(): ?string
    {
        return $this->keyFacebook;
    }

    public function setKeyFacebook(?string $keyFacebook): self
    {
        $this->keyFacebook = $keyFacebook;

        return $this;
    }

    public function getKeyGoogle(): ?string
    {
        return $this->keyGoogle;
    }

    public function setKeyGoogle(?string $keyGoogle): self
    {
        $this->keyGoogle = $keyGoogle;

        return $this;
    }

    public function getBirthday(): ?string
    {
        return $this->birthday;
    }

    public function setBirthday(?string $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getLastActivity(): ?string
    {
        return $this->lastActivity;
    }

    public function setLastActivity(?string $lastActivity): self
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getEmailValidated(): ?bool
    {
        return $this->emailValidated;
    }

    public function setEmailValidated(?bool $emailValidated): self
    {
        $this->emailValidated = $emailValidated;

        return $this;
    }

    public function getValidated(): ?bool
    {
        return $this->validated;
    }

    public function setValidated(bool $validated): self
    {
        $this->validated = $validated;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getEmailSecretCode(): ?string
    {
        return $this->emailSecretCode;
    }

    public function setEmailSecretCode(?string $emailSecretCode): self
    {
        $this->emailSecretCode = $emailSecretCode;

        return $this;
    }

    public function getChangeUsername(): ?string
    {
        return $this->changeUsername;
    }

    public function setChangeUsername(?string $changeUsername): self
    {
        $this->changeUsername = $changeUsername;

        return $this;
    }

    public function getChangeUsernameDate(): ?string
    {
        return $this->changeUsernameDate;
    }

    public function setChangeUsernameDate(?string $changeUsernameDate): self
    {
        $this->changeUsernameDate = $changeUsernameDate;

        return $this;
    }

    public function getIdMemberRole(): ?MemberRole
    {
        return $this->idMemberRole;
    }

    public function setIdMemberRole(?MemberRole $idMemberRole): self
    {
        $this->idMemberRole = $idMemberRole;

        return $this;
    }

    // Metodos de interfaces de autorización
    public function getRoles()
    {
        return array($this->idMemberRole->getName());
    }

    public function getSalt()
    {
        // TODO: Implement getSalt() method.
        return false;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function serialize()
    {
        $isActive = 1;
        if($this->deleted == 1){
            $isActive = 0;
        }
        return serialize(array(
            $this->idMember,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
            $this->deleted,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->idMember,
            $this->username,
            $this->password,
            //$this->deleted
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        //return $this->isActive;
        if($this->deleted == 0){
            return 1;
        }
        return $this->deleted;
    }

    /**
     * @return bool whether the user is active or not
     */
    public function isActiveNow()
    {
        $delay = new \DateTime('2 minutes ago');
        return ($this->getlastActivity() > $delay->getTimestamp());
    }

    /**
     * @return bool whether the user is active or not
     */
    public function canBeLogged()
    {
       if ($this->getDeleted()) { return false; }
       if (!$this->getEmailValidated()) { return false; }
       if (!$this->getValidated()) { return false; }
       if (!$this->getIsActive()) { return false; }
       return true;
    }
}