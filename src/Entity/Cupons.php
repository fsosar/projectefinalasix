<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cupons
 *
 * @ORM\Table(name="cupons")
 * @ORM\Entity
 */
class Cupons
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_cupon", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCupon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=2, nullable=true)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var int|null
     *
     * @ORM\Column(name="used", type="integer", nullable=true)
     */
    private $used;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    public function getIdCupon(): ?int
    {
        return $this->idCupon;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getUsed(): ?int
    {
        return $this->used;
    }

    public function setUsed(?int $used): self
    {
        $this->used = $used;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }


}
