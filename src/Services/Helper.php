<?php
namespace App\Services;

use App\Entity\MemberActivity;
use App\Entity\Member;
use App\Entity\Transaction;
use DateTime;

class Helper
{
    public $em;

    public function __construct($em = null, $token = null)
    {
        $this->em = $em;
    }
    
    // Registrar en la tabla activity que este usuario ha querido app_member_edit la contraseña
    public function memberActivity($em, $keyActivity, $idMember)
    {
        $created = new \Datetime("now");

        $memberActivity = new MemberActivity();

        $memberActivity->setKeyActivity($keyActivity);

        $idMember = $em->getRepository(Member::class)->findOneBy(array('idMember' => $idMember));
        $memberActivity->setIdMember($idMember);

        $memberActivity->setFecha($created->getTimestamp());
        $em->persist($memberActivity);
        $em->flush();
    }

    // Limpiar los GET y POST que nos llegan para evitar códigos "Ataques"
    public function sanitize($input)
    {
        if (is_array($input)) {
            foreach ($input as $var => $val) {
                $output[$var] = $this->sanitize($val);
            }
        } else {
            if (get_magic_quotes_gpc()) {
                $input = stripslashes($input);
            }
            $input = self::cleanInput($input);

        }
        return $input;
    }

    public function cleanInput($input)
    {
        $search = array(
            '@<script[^>]*?>.*?</script>@si', // Elimina javascript
            '@<[\/\!]*?[^<>]*?>@si', // Elimina las etiquetas HTML
            '@<style[^>]*?>.*?</style>@siU', // Elimina las etiquetas de estilo
            '@<![\s\S]*?--[ \t\n\r]*>@', // Elimina los comentarios multi-línea
        );
        $output = preg_replace($search, '', $input);
        return $output;
    }

    public function getNumeroFactura($idMember)
    {
        $em = $this->em;
        $numeroFactura = null;
        $fechaActual = new DateTime();
        $year = $fechaActual->format('Y');

        $facturacion = $em->getRepository(Transaction::class)->findOneBy(array('finished' => 1),array('facturationNumber' => 'DESC'));

        ///////////////////////////////////////////////////////////////////////////////////////////////

        if (!$facturacion) {
            $numeroFactura = "A000001";
            return $numeroFactura . '-' . $year;
        } else {
            $facturationDate = explode("-",$facturacion->getFacturationNumber());
            if($facturationDate[1] == $year){
                $numeroFactura = $facturationDate[0];
                $numeroFactura++;
                $numeroFactura == strval($numeroFactura);    
            } else {
                $numeroFactura = "A000001";
                return $numeroFactura . '-' . $year;
            }

        }

        switch (strlen($numeroFactura)) {
            case 1:
            $numeroFactura = "A00000" . $numeroFactura;
            break;
            case 2:
            $numeroFactura = "A0000" . $numeroFactura;
            break;
            case 3:
            $numeroFactura = "A000" . $numeroFactura;
            break;
            case 4:
            $numeroFactura = "A00" . $numeroFactura;
            break;
            case 5:
            $numeroFactura = "A0" . $numeroFactura;
            break;
            case 6:
            $numeroFactura = "A" . $numeroFactura;
            break;
        }
        $numeroFactura = $numeroFactura . '-' . $year;
        
        return $numeroFactura;
    }
    // END Limpiar los GET y POST que nos llegan para evitar códigos "Ataques"
}
