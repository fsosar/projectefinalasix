<?php
namespace App\Services;



class ManagerMail
{
    private $from = 'sebasti042@gmail.com';
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function templeteOne($parameters): bool
    {
        $to = $parameters['to'];
        $subject = $parameters['subject'];
        $body = $parameters['body'];

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($this->from)
            ->setTo($to)
            ->setBody($body, "text/html");
        ;
        return $this->mailer->send($message);
        
    }
}
