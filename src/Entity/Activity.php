<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity
 */
class Activity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_activity", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idActivity;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_member", type="integer", nullable=true)
     */
    private $idMember;

    /**
     * @var string|null
     *
     * @ORM\Column(name="route", type="string", length=100, nullable=true)
     */
    private $route;

    /**
     * @var string|null
     *
     * @ORM\Column(name="params", type="string", length=500, nullable=true)
     */
    private $params;

    /**
     * @var string|null
     *
     * @ORM\Column(name="request", type="text", length=65535, nullable=true)
     */
    private $request;

    /**
     * @var string|null
     *
     * @ORM\Column(name="wildcard", type="string", length=50, nullable=true)
     */
    private $wildcard;

    /**
     * @var string|null
     *
     * @ORM\Column(name="locale", type="string", length=2, nullable=true)
     */
    private $locale;

    /**
     * @var int|null
     *
     * @ORM\Column(name="date", type="bigint", nullable=true)
     */
    private $date;

    public function getIdActivity(): ?int
    {
        return $this->idActivity;
    }

    public function getIdMember(): ?int
    {
        return $this->idMember;
    }

    public function setIdMember(?int $idMember): self
    {
        $this->idMember = $idMember;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getRequest(): ?string
    {
        return $this->request;
    }

    public function setRequest(?string $request): self
    {
        $this->request = $request;

        return $this;
    }

    public function getWildcard(): ?string
    {
        return $this->wildcard;
    }

    public function setWildcard(?string $wildcard): self
    {
        $this->wildcard = $wildcard;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(?string $date): self
    {
        $this->date = $date;

        return $this;
    }


}
