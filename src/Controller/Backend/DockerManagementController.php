<?php

namespace App\Controller\Backend;

use App\Entity\Activity;
use App\Entity\Member;
use App\Entity\Docker;
use App\Entity\MemberRole;
use App\Entity\MemberMeta;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\VarDumper\VarDumper;

class DockerManagementController extends Controller
{
    public const PORT_NUMBER = 10000;

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/list")
     * 
     */
    public function dockerList(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $userDockers = $em->getRepository(Docker::class)->findBy(['member' => $user->getIdMember()]);

        return $this->render('backend/dockermanagement/list.html.twig', [
            'userDockers' => $userDockers,
        ]);
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/init")
     * 
     */
    public function dockerInitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $response = new JsonResponse();

        //PARAMETERS OBTAINED FROM REQUEST
        $parameters = [];
        foreach ($request->request->all() as $key => $item) {
            if ($key != '_csrf_token') {
                $parameters[$key] = $helper->sanitize($item);
            }
        };

        $idDocker = $parameters['idDocker'];

        //dump($parameters);die();

        try{
            $dockerToInit = $em->getRepository(Docker::class)->findOneBy(['idDocker' => $idDocker]);

            //AÑADIR AQUI LAS EJECUCION DE LOS SCRIPTS DE PYTHON
            $processExecution = ['python', 'pythonscripts/initDocker.py',];

            if($dockerToInit) array_push($processExecution,"--idname={$dockerToInit->getIdDocker()}");
            else throw new Exception('Docker does not exists');

            $process = new Process($processExecution);
            $process->run();

            if($process->getOutput()){
                //$em->remove($dockerToDelete);
                //$em->flush();   
            }else{
                throw new Exception();
            }

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success',
                'msg' => 'Docker initialized successfully',
            ]);
        
        } catch (Exception $e) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Error, docker could not be initialized. Try again later',
            ]);
        }

        return $response;
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/stop")
     * 
     */
    public function dockerStopAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $response = new JsonResponse();

        //PARAMETERS OBTAINED FROM REQUEST
        $parameters = [];
        foreach ($request->request->all() as $key => $item) {
            if ($key != '_csrf_token') {
                $parameters[$key] = $helper->sanitize($item);
            }
        };

        $idDocker = $parameters['idDocker'];

        try{
            $dockerToStop = $em->getRepository(Docker::class)->findOneBy(['idDocker' => $idDocker]);

            //AÑADIR AQUI LAS EJECUCION DE LOS SCRIPTS DE PYTHON
            $processExecution = ['python', 'pythonscripts/stopDocker.py',];

            if($dockerToStop) array_push($processExecution,"--idname={$dockerToStop->getIdDocker()}");
            else throw new Exception('Docker does not exists');

            $process = new Process($processExecution);
            $process->run();

            if($process->getOutput()){
                //$em->remove($dockerToStop);
                //$em->flush();   
            }else{
                throw new Exception();
            }

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success',
                'msg' => 'Docker stopped successfully',
            ]);
        
        } catch (Exception $e) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Error, docker could not be stopped. Try again later',
            ]);
        }

        return $response;
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/delete")
     * 
     */
    public function dockerDeleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $response = new JsonResponse();

        //PARAMETERS OBTAINED FROM REQUEST
        $parameters = [];
        foreach ($request->request->all() as $key => $item) {
            if ($key != '_csrf_token') {
                $parameters[$key] = $helper->sanitize($item);
            }
        };

        $idDocker = $parameters['idDocker'];

        //dump($parameters);die();

        try{
            $dockerToDelete = $em->getRepository(Docker::class)->findOneBy(['idDocker' => $idDocker]);
            
            if (!$dockerToDelete) {
                throw new Exception('Docker does not exists');
            }else{

                //AÑADIR AQUI LAS EJECUCION DE LOS SCRIPTS DE PYTHON

                $processExecution = ['python', 'pythonscripts/stopDocker.py',];

                if($dockerToDelete) array_push($processExecution,"--idname={$dockerToDelete->getIdDocker()}");
                else throw new Exception('');

                $process = new Process($processExecution);
                $process->run();

                $processExecution = ['python', 'pythonscripts/rmDocker.py',];

                if($dockerToDelete) array_push($processExecution,"--idname={$dockerToDelete->getIdDocker()}");
                else throw new Exception('Docker does not exists');

                $process = new Process($processExecution);
                $process->run();

                if($process->getOutput()){
                    $em->remove($dockerToDelete);
                    $em->flush();   
                }else{
                    throw new Exception();
                }
            }

            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success',
                'msg' => 'Docker deleted successfully',
            ]);
        
        } catch (Exception $e) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Error, docker could not be deleted. Try again later',
            ]);
        }

        return $response;
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/console/{idDocker}")
     * 
     */
    public function dockerConsole(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('backend/dockermanagement/dockerconsole.html.twig');
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/create")
     * 
     */
    public function dockerCreate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('backend/dockermanagement/create.html.twig');
    }

    /**
     * @Route("/{_locale}/dashboard/dockermanagement/add")
     * 
     */
    public function dockerAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $response = new JsonResponse();

        //PARAMETERS AND DATA TO SAVE TO DB
        $parameters = [];
        foreach ($request->request->all() as $key => $item) {
            if ($key != '_csrf_token') {
                $parameters[$key] = $helper->sanitize($item);
            }
        };

        $dockerProperty = "";
        $dockerMember = "";
        $dockerName = "";
        $dockerPort = [];
        $dockerId = "";
        
        try{
            if (!array_key_exists('member', $parameters) && $parameters['member'] == '') 
                $parameters['member'] = $this->get('security.token_storage')->getToken()->getUser();

            $user = $em->getRepository(Member::class)->findOneBy(['idMember' => $parameters['member']]);
            $userDockerMaxCount = $user->getIdMemberRole()->getDockerMaxCount();

            //SET MEMBER TO ADD TO DOCKER ROW
            //DOCKERS THE USER HAS
            $dockersUser = $em->getRepository(Docker::class)->findBy(['member' => $user->getIdMember()]);
            //VERIFIES IS THE USER EXCEEDED THE AMOUNT OF DOCKERS
            if (count($dockersUser) >= $userDockerMaxCount)
                throw new \Exception('Max quantity of docker creation reached!');
            
            $dockerMember = $user;
            
            //SET PORT TO ADD TO DOCKER ROW
            $portToDocker = 10001;
            $maxPortToDocker = 90000;
            $assignedPorts = 0;
            
            for ($i=10001; $i <= $maxPortToDocker ; $i++) {
                if(!$em->getRepository(Docker::class)->isDockerPortTaken($i)){

                    switch ($assignedPorts) {
                        case 0:
                            $dockerPort['db'] = $i;
                            break;
                        case 1:
                            $dockerPort['ssh'] = $i;
                            break;
                        case 2:
                            $dockerPort['adminer'] = $i;
                            break;
                    }
                    
                    $assignedPorts = count($dockerPort);

                    if($assignedPorts == 3)
                        break;
                }
            }
            $dockerPort = json_encode($dockerPort);
            //dump($dockerPort);die();
            /*
            $lastDockerPort = $em->getRepository(Docker::class)->findOneBy([], ['port' => 'DESC']);
            if ($lastDockerPort){
                if (!$lastDockerPort%10)
                    $portToDocker = (int)substr_replace($lastDockerPort, '0', -3, 3);
                $portToDocker = $lastDockerPort->getPort() + 10;
            }
            $dockerPort = $portToDocker;
            */


            //SET DOCKER ID NAME TO ADD TO DOCKER ROW
            $dockerId = $em->getRepository(Docker::class)->createDockerId();

            //SET DOCKER NAME TO ADD TO DOCKER ROW
            $dockerName = $parameters['name'];

            //SET DOCKER PROPERTY TO ADD TO DOCKER ROW
            $dockerProperty = $parameters['type'];

            $docker = new Docker();
            $docker->setMember($dockerMember);
            $docker->setIdDocker($dockerId);
            $docker->setName($dockerName);
            $docker->setPort($dockerPort);
            $docker->setDockerProperties($dockerProperty);

            //PYTHON SCRIPT EXECUTION USING THE DOCKER DATA SAVED IN DB
            $processExecution = ['python', 'pythonscripts/createDocker.py',];

            if($dockerPort) array_push($processExecution,"--port={$dockerPort}");
            if($dockerId) array_push($processExecution,"--idname={$dockerId}");
            if($dockerName) array_push($processExecution,"--dockername={$dockerName}");
            if($dockerPort) array_push($processExecution,"--dockerproperty={$dockerProperty}");

            $process = new Process($processExecution);
            $process->run();


            if($process->getOutput()){
                if(!substr(rtrim($process->getOutput()), -1))
                    throw new Exception('');
            }else{
                throw new Exception('');
            }

            $em->persist($docker);
            $em->flush();
            
            $response->setStatusCode(200);
            $response->setData([
                'response' => 'success',
                'msg' => 'Docker created successfully',
            ]);
        } catch (Exception $e) {
            dump($e);die();
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => 'Error, docker could not be created. Try again later',
            ]);
        }

        return $response;
    }
    
    /**
     * @Route("/{_locale}/dashboard/prueba")
     * 
     */
    public function pruebaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $message = "";

        $memberRole = $em->getRepository(MemberRole::class)->findAll();

        //GET USER LOGGED IN AND THE MAX QUANTITY OF DOCKERS ALLOWED
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $userDockerMaxCount = $user->getIdMemberRole()->getDockerMaxCount();

        //DOCKERS THE USER HAS
        $dockersUser = $em->getRepository(Docker::class)->findBy(['member' => $user->getIdMember()]);

        if (count($dockersUser) >= $userDockerMaxCount) {
            $message = "Max quantity of docker creation reached!";
        }

        //RANDOM STRING NAME :: PROVISIONAL
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        //RANDOM PORT ADD :: PROVISIONAL
        $portToDocker = 10000;
        $lastDockerPort = $em->getRepository(Docker::class)->findOneBy([], ['port' => 'DESC']);
        if ($lastDockerPort) 
            $portToDocker = $lastDockerPort->getPort() + 1;

        $docker = new Docker();
        $docker->setMember($user);
        $docker->setName($randomString);
        $docker->setPort($portToDocker);
        $docker->setCount(1);
        
        $em->persist($docker);
        $em->flush();

        //dump($dockersUser);die();

        return $this->render('backend/script/index.html.twig', [
            'test' => $docker,
        ]);
    }

}
