<?php

namespace App\Repository;

use App\Entity\Docker;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\VarDumper\VarDumper;

class DockerRepository extends EntityRepository
{

    public function createDockerId()
    {

        $dockerIdValid = false;
        $idDockerName = '';
        
        do {
            //RANDOM STRING NAME FOR DOCKER ID
            $length = 10;
            $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $idDockerName = $randomString;

            //ASKING DATABASE IF NAME IS TAKEN
            $dockerIdInDatabase = $this->findOneBy(['idDocker' => $randomString]);
            
            if(!$dockerIdInDatabase){
                $dockerIdValid = true;
            }

        } while (!$dockerIdValid);

        return $idDockerName;
    }

    public function isDockerPortTaken($dockerPort)
    {

        $dockerPortIsTaken = false;
        $idDockerName = '';

        $qb = $this->createQueryBuilder('d')
        ->andWhere('d.port LIKE :portsearch')
        ->setParameter('portsearch', '%'.$dockerPort.'%')
        ->getQuery()
	->execute();

        //ASKING DATABASE IF NAME IS TAKEN
        $dockerIdInDatabase = $qb;

        if(!empty($dockerIdInDatabase)) $dockerPortIsTaken = true;

        return $dockerPortIsTaken;
    }

    public function search($parameter)
    {
        $username = $parameter["username"];
        $idMemberRole = $parameter["idMemberRole"];
        $name = $parameter["name"];
        $emailValidated = $parameter["emailValidated"];
        $validated = $parameter["validated"];
        $isActive = $parameter["isActive"];
        $deleted = $parameter["deleted"];

        $qb = $this->createQueryBuilder('m');

        if ($username) {
            $qb->andWhere('m.username like :username')->setParameter('username', '%' . $username . '%');
        }

        if ($name) {
            $qb->andWhere('m.name like :name')->setParameter('name', '%' . $name . '%');
        }

        if ($idMemberRole) {
            $qb->andWhere('m.idMemberRole = :idMemberRole')->setParameter('idMemberRole', $idMemberRole);
        }

        if ($emailValidated) {
            $qb->andWhere('m.emailValidated = :emailValidated')->setParameter('emailValidated', $emailValidated);
        }

        if ($isActive) {
            $qb->andWhere('m.isActive = :isActive')->setParameter('isActive', $isActive);
        }

        if ($validated) {
            $qb->andWhere('m.validated = :validated')->setParameter('validated', $validated);
        }

        if ($deleted) {
            $qb->andWhere('m.deleted = :deleted')->setParameter('deleted', $deleted);
        }

        return $qb->getQuery()->execute();
    }

}
