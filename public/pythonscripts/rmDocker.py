#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys    # per importar la llibreria necessària
import datetime
import subprocess
args = sys.argv
import os
import json
del args[0]

docker={}

for elem in args:
        if elem.startswith("--"):
                elem = elem.replace("--","")
                elem = elem.split("=")
                argex = elem[0]
                arval = elem[1]
                docker[argex] = arval
print(docker)

dockerstart = "/home/projecte/dockervol/"+docker["idname"]+".yml"
dockerdir = "/home/projecte/dockervol/"+docker["idname"]

proc = os.system("docker stack rm "+docker["idname"])
proc2 = os.system("rm -r "+dockerdir+" && rm "+dockerstart)

