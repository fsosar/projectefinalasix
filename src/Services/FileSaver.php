<?php
namespace App\Services;

use Aws\S3\S3Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FileSaver
{
    /**
     * Variable Container que nos permitirá acceder a la configuración en el archivo config.yml
     */
    private $container;

    /**
     * Booleano que nos dice si està activado o no el s3
     */
    private $s3;

    /**
     * Cliente s3 en caso de que esté activado
     */
    private $s3Client;

    /**
     * Bucket con el que interaccionar si el s3 está activado
     */
    private $bucket;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        if ($this->checkS3()) {
            $this->s3 = true;
            $this->setS3Client();
        } else {
            $this->s3 = false;
        }

    }
    /**
     * Guarda un fitxer/base64 al S3 o local
     *
     * @param File $file Fitxer del que agafar l'extensió de l'usuari i/o s'ha de guardar
     * @param String $path Ruta on guardar el fitxer sense el nom del mateix
     * @param Boolean $private Diu si el fitxer será public o privat si s'está guardant dins del S3
     * @param String $base64 String amb la base64 del que es crearà el fitxer a pujar al servidor/s3
     *
     * @return Response
     *
     *
     */
    public function saveFile($file, $path, $args = null)
    {
        $filename = "";

        $private = false;
        $base64 = null;

        if (isset($args) && gettype($args) == "array") {
            if (isset($args["private"])) {
                $private = $args["private"];
            }
            if (isset($args["base64"])) {
                $private = $args["base64"];
            }
        }

        if ($this->s3) {

            if (!file_exists("temp/")) {
                mkdir("temp");
            }

            $filename = uniqid() . "." . $file->getClientOriginalExtension();

            if (!is_null($base64) && !empty($base64)) {

                $base_to_php = explode(',', $base64);
                $data = base64_decode($base_to_php[1]);

                file_put_contents("temp/$filename", $data); // move the file to a path

            } else {

                $file->move("temp/", $filename);

            }

            $key = $path . $filename;

            if (!file_exists("temp/$filename")) {
                throw new \Exception("The temporary file couldn't be created, please contact with the technical department.");
            }

            $cliente = $this->s3Client;

            $settings = array(
                'Bucket' => $this->bucket,
                'Key' => $key,
                'SourceFile' => "temp/$filename",
            );

            if (!$private) {
                $settings["ACL"] = 'public-read';
            }

            try {

                $result = $cliente->putObject($settings);

            } catch (S3Exception $e) {
                unlink("temp/$filename");
                throw new \Exception($e->getMessage());
            }

            unlink("temp/$filename");

        } else {

            $filename = uniqid() . "." . $file->getClientOriginalExtension();

            $this->checkExistsLocal($path);

            if (!is_null($base64) && !empty($base64)) {

                $base_to_php = explode(',', $base64);
                $data = base64_decode($base_to_php[1]);

                file_put_contents($path . $filename, $data); // move the file to a path

            } else {

                $file->move($path, $filename);

            }

            if (!file_exists($path . $filename)) {
                throw new \Exception("The file couldn't be uploaded, please contact with the technical department.");
            }

        }

        return $filename;
    }

    public function deleteFileS3($key)
    {
        if ($this->s3) {

            $cliente = $this->s3Client;

            if ($cliente->doesObjectExist($this->bucket, $key)) {

                try {

                    $cliente->deleteObject([
                        'Bucket' => $this->bucket,
                        'Key' => $key,
                    ]);

                    if ($cliente->doesObjectExist($this->bucket, $key)) {
                        throw new \Exception("The file couldn't be deleted. Contact with the technical department.");
                    }

                } catch (S3Exception $e) {
                    throw new \Exception($e->getMessage());
                }

            } else {
                throw new \Exception("This file couldn't be deleted because it doesn't exist.");
            }

        } else {
            if (file_exists($key)) {
                unlink($key);
                if (file_exists($key)) {
                    throw new \Exception("The file couldn't be deleted. Contact with the technical department.");
                }
            }

        }

        return true;
    }

    public function showFileS3($key)
    {
        $cliente = $this->s3Client;

        if ($cliente->doesObjectExist($this->bucket, $key)) {

            try {
                // Get the object.
                $result = $cliente->getObject([
                    'Bucket' => $this->bucket,
                    'Key' => $key,
                ]);

                // Display the object in the browser.
                header("Content-Type: {$result['ContentType']}");
                echo $result['Body'];
            } catch (S3Exception $e) {
                throw new \Exception("The file couldn't be uploaded, please contact with the technical department refeering this message: " . $e->getMessage());
            }
        } else {
            throw new \Exception("This file doesn't exist.");
        }
    }

    public function checkExistsLocal($path)
    {
        $ruta = "";
        $path = explode("/", $path);
        foreach ($path as $carpeta) {
            $ruta .= $carpeta . "/";
            if (!file_exists($ruta)) {
                mkdir($ruta);
            }
        }
    }

    private function setS3Client()
    {
        $profile = $this->getProfile();
        $region = $this->getRegion();
        $bucket = $this->getBucketName();

        if (empty($profile) || is_null($profile) || empty($region) || is_null($region) || empty($bucket) || is_null($bucket)) {
            throw new \Exception("Por favor declara \"amazon.s3\", \"amazon.s3.profile\", \"amazon.s3.region\", \"amazon.s3.bucket\", en el fichero config.yml!");
        }

        $this->s3Client = new S3Client([
            'profile' => $profile,
            'version' => 'latest',
            'region' => $region,
        ]);
        $this->bucket = $bucket;
    }

    /**
     * Comprueba que exista la variable amazon.s3 en el config.yml y devuelve su valor si existe
     */
    public function checkS3()
    {
        if ($this->container->hasParameter('amazon.s3')) {
            return $this->container->getParameter('amazon.s3');
        }
        return false;
    }

    /**
     * Comprueba que exista la variable amazon.s3.bucket en el config.yml y devuelve su valor si existe
     */
    private function getBucketName()
    {
        if ($this->container->hasParameter('amazon.s3.bucket')) {
            return $this->container->getParameter('amazon.s3.bucket');
        }
        return false;
    }

    /**
     * Comprueba que exista la variable amazon.s3.profile en el config.yml y devuelve su valor si existe
     */
    private function getProfile()
    {
        if ($this->container->hasParameter('amazon.s3.profile')) {
            return $this->container->getParameter('amazon.s3.profile');
        }
        return false;
    }

    /**
     * Comprueba que exista la variable amazon.s3.region en el config.yml y devuelve su valor si existe
     */
    private function getRegion()
    {
        if ($this->container->hasParameter('amazon.s3.region')) {
            return $this->container->getParameter('amazon.s3.region');
        }
        return false;
    }

}
