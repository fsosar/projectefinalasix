<?php
namespace App\EventListener;

use App\Entity\Activity;
use App\Entity\Member;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;

class ActivityListener
{
    public $em, $token, $router, $security;

    public function __construct($em = null, $token = null, $router = null, $security = null)
    {
        $this->em = $em;
        $this->token = $token;
        $this->router = $router;
        $this->security = $security;
    }

    /**
     * Update the user "lastActivity" on each request
     * @param FilterControllerEvent $event
     */
    public function onCoreController(FilterControllerEvent $event)
    {

        // Here we are checking that the current request is a 'MASTER_REQUEST', and ignore any subrequest in the process (for example when doing a render() in a twig template)
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }

        if ($this->token->getToken()) {
            $member = $this->token->getToken()->getUser();

            $date = new \DateTime('now');
            if (($member instanceof Member) && !($member->isActiveNow())) {
                $member->setLastActivity($date->getTimestamp());
                $this->em->flush($member);

            }
            if (($member instanceof Member) && !$member->canBeLogged() && !$this->security->isGranted('ROLE_PREVIOUS_ADMIN')) {
                // new RedirectResponse($this->router->generate('app_frontend_member_logout'));
                $locale = $event->getRequest()->attributes->all()['_locale'];
                header('Location: /'.$locale.'/logout');
                die();
            }
        }

        // Registrar la ruta
        $route = $event->getRequest()->attributes->all()['_route'];
        $activitiesJson = file_get_contents('route.json');
        $activities = json_decode($activitiesJson, true);

        foreach ($activities as $key => $value) {
            if ($key != 'NotController') {
                for ($a = 0; $a < count($value); $a++) {
                    if ($value[$a]['route'] == $route) {
                        $activity = new Activity();
                        if ($member instanceof Member) {
                            $activity->setIdMember($member->getIdMember());
                        }
                        $activity->setRoute($route);
                        $activity->setDate($date->getTimestamp());
                        if (isset($event->getRequest()->attributes->all()['_locale'])) {
                            $activity->setLocale($event->getRequest()->attributes->all()['_locale']);
                        }
                        $activity->setParams(json_encode($event->getRequest()->attributes->all()['_route_params'], JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK));
                        $activity->setRequest(json_encode($event->getRequest()->request->all(), JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK));

                        // Añadimos el campo comodín si es que existe
                        if (isset($value[$a]['wildcard'])) {
                            if (isset($event->getRequest()->request->all()[$value[$a]['wildcard']])) {
                                $activity->setWildcard($event->getRequest()->request->all()[$value[$a]['wildcard']]);
                            } else if (isset($event->getRequest()->attributes->all()[$value[$a]['wildcard']])) {
                                $activity->setWildcard($event->getRequest()->attributes->all()[$value[$a]['wildcard']]);
                            }
                        }
                        $this->em->persist($activity);
                        $this->em->flush();
                    }
                }
            }
        }
    }
}
