<?php

namespace App\Controller\Frontend;

use App\Entity\BlockedStock;
use App\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class ProductController extends Controller
{

    /**
     * @Route("/{_locale}/product")
     */
    public function buscadorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $prod = $this->get('app.services_product');


        $localeProduct = $request->get('localeProduct');
        $filtreMinPrice = $request->get('filtreMinPrice');
        $filtreMaxPrice = $request->get('filtreMaxPrice');
        $filtreCategory = $request->get('category');
  
        if (!$localeProduct) {
            $localeProduct = $request->getLocale();
        }
        
        $product = $prod->searchProduct([
            'em' => $em,
            'localeProduct' => $localeProduct,
            'idProduct' => null,
            'filtreMinPrice' => $filtreMinPrice,
            'filtreMaxPrice' => $filtreMaxPrice,
            'filtreCategory' => $filtreCategory,
            'filtreFrom' => null,
            'filtreTo' => null
        ]);

        $blockedStock = $em->getRepository(BlockedStock::class)->findAll();
        
        //Aplicació del bundle KnpPaginator
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $product, 
            $request->query->getInt('page', 1),
            10
        );

        $category = $em->getRepository(Category::class)->findBy(['categoryKey' => 'product_level_1']);

        return $this->render('frontend/product/buscador.html.twig', [
            'pagination' => $pagination,
            'filtreMinPrice' => $filtreMinPrice,
            'filtreMaxPrice' => $filtreMaxPrice,
            'category' => $category,
            'blockedStock' => $blockedStock
        ]);
    }
}
