<?php

namespace App\Controller\Backend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    const keyActivity = 'default';

    /**
     * @Route("/test")
     */
    public function pruebasAction(Request $request)
    {
        return new Response(1);
    }
    /**
     * @Route("/test1")
     */
    public function testAction(Request $request)
    {
        $this->get('app.manager_activity')->simple(array(
            'keyActivity' => self::keyActivity . '_test',
            'id' => null,
            'role' => 'ROLE_ADMINISTRADOR',
            'details' => array('test' => 'test'),
        ));

        die();
    }

    /**
     * @Route("prueba/s3")
     */
    public function s3Action(Request $request)
    {
        return $this->render('frontend/pruebas.html.twig');
    }

    /**
     * @Route("api/prueba/s3")
     */
    public function apiS3Action(Request $request)
    {
        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => 'Something unexpected happened, please contact the technical department.',
        ));

        $file = $request->files->get('upload-fichero');

        $fileSaver = $this->get('app.file_saver');

        if (!is_null($file)) {
            
            try {
                $respuesta = $fileSaver->saveFile($file, 'img/prueba/', $args);

                if ($respuesta) {
                    $response->setStatusCode(200);
                    $response->setData(array(
                        'response' => 'success',
                        'msg' => 'File uploaded successfuly.',
                    ));
                }

            } catch (\Exception $e) {

                $response->setData(array(
                    'response' => 'error',
                    'msg' => $e->getMessage(),
                ));

            }
        }

        return $response;
    }

    /**
     * @Route("api/delete/s3")
     */
    public function apiDeleteS3Action(Request $request)
    {
        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => 'Something unexpected happened, please contact the technical department.',
        ));

        $file = $request->get('key-fichero');

        $fileSaver = $this->get('app.file_saver');

        try {

            if ($fileSaver->deleteFileS3($file)) {
                $response->setStatusCode(200);
                $response->setData(array(
                    'response' => 'success',
                    'msg' => 'File deleted successfuly.',
                ));
            }

        } catch (\Exception $e) {

            $response->setData(array(
                'response' => 'error',
                'msg' => $e->getMessage(),
            ));

        }

        return $response;
    }



    /**
     * @Route("api/show/s3")
     */
    public function apiShowPrivateS3Action(Request $request)
    {
        $response = new JsonResponse();
        $response->setStatusCode(500);
        $response->setData(array(
            'response' => 'error',
            'msg' => 'Something unexpected happened, please contact the technical department.',
        ));


        $fileSaver = $this->get('app.file_saver');

        $key = 'img/prueba/5cf8cbe61585d.jpg';
        try {

            $fileSaver->showFileS3($key);
            

        } catch (\Exception $e) {

            $response->setData(array(
                'response' => 'error',
                'msg' => $e->getMessage(),
            ));

        }

        return $response;
    }
}
