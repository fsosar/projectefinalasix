<?php

namespace App\Controller\Backend;

use App\Entity\Activity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ActivityController extends Controller
{

    /**
     * @Route("/{_locale}/dashboard/activity/search")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $filterRoute = $helper->sanitize($request->request->get('filterRoute'));
        $filterIdMember = $helper->sanitize($request->request->get('filterIdMember'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            } else {
                $parameters = [];
                if ($filterRoute) {
                    $parameters['route'] = $filterRoute;
                }
                if ($filterIdMember) {
                    $parameters['idMember'] = $filterIdMember;
                }

                $activity = $em->getRepository(Activity::class)->findBy($parameters, ['date' => 'DESC']);

                $jsonFile = file_get_contents('route.json');
                $activityFile = json_decode($jsonFile, true);

                foreach ($activity as $item) {
                    foreach ($activityFile as $controller) {
                        foreach ($controller as $route) {
                            if ($item->getRoute() == $route['route']) {
                                $item->setRoute($route['description']);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Error');
        }

        if (!empty($responseMsgError)) {
            echo "Error";
            die();
        } else {
            return $this->render('backend/activity/management/table.html.twig', [
                'activity' => $activity,
            ]);
        }

    }

    /**
     * @Route("/{_locale}/dashboard/activity/management")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function managementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $json = file_get_contents("route.json");
        $activity = json_decode($json, true);

        return $this->render('backend/activity/management/html.html.twig', [
            'activity' => $activity,
        ]);
    }

}
