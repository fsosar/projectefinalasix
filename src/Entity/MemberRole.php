<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MemberRole
 *
 * @ORM\Table(name="member_role")
 * @ORM\Entity
 */
class MemberRole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_Member_Role", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMemberRole;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="docker_max_count", type="integer", nullable=true)
     */
    private $dockerMaxCount;

    public function getIdMemberRole(): ?int
    {
        return $this->idMemberRole;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDockerMaxCount(): ?int
    {
        return $this->dockerMaxCount;
    }

    public function setDockerMaxCount(?int $dockerMaxCount): self
    {
        $this->dockerMaxCount = $dockerMaxCount;

        return $this;
    }


}
