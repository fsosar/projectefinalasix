#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys    # per importar la llibreria necessària
import datetime
import subprocess
args = sys.argv
import os
import json
del args[0]

docker={}

proc=""

for elem in args:
	if elem.startswith("--"):
		elem = elem.replace("--","")
		elem = elem.split("=")
		argex = elem[0]
		arval = elem[1]
		docker[argex] = arval
ports = json.loads(docker['port'])

plantilla1="version: '3.1'\n\nservices:\n\n  db:\n    image: mariadb:latest\n    user: root\n    restart: always\n    ports:\n      - "+str(ports["db"])+":3306\n    environment:\n      MYSQL_ROOT_PASSWORD: secret\n    volumes:\n      - /home/projecte/dockervol/"+str(docker["idname"])+":/var/lib/mysql\n\n    deploy:\n      placement:\n        constraints: [node.hostname == sebypau]\n\n  adminer:\n    image: adminer:latest\n    restart: always\n    ports:\n      - "+str(ports["adminer"])+":8080"
plantilla2="version: '3.2'\n\nservices:\n\n  db:\n    image: postgres:latest\n    ports:\n      - "+str(ports["db"])+":5432\n      - "+str(ports["ssh"])+":22\n    environment:\n      POSTGRES_PASSWORD: secret\n    volumes:\n      - /home/projecte/dockervol/"+str(docker["idname"])+":/var/lib/postgresql/data\n\n    deploy:\n      placement:\n        constraints: [node.hostname == sebypau]\n\n  adminer:\n    image: adminer:latest\n    restart: always\n    ports:\n      - "+str(ports["adminer"]) +":8080"
plantilla3="version: '3.1'\n\nservices:\n\n  db:\n    image: mysql:latest\n    user: root\n    restart: always\n    ports:\n      - "+str(ports["db"])+":3306\n    environment:\n      MYSQL_ROOT_PASSWORD: secret\n    volumes:\n      - /home/projecte/dockervol/"+str(docker["idname"]) +":/var/lib/mysql\n\n    deploy:\n      placement:\n        constraints: [node.hostname == sebypau]\n\n  adminer:\n    image: adminer:latest\n    restart: always\n    ports:\n      - "+str(ports["adminer"])+":8080"
plantilla4="version: '3.1'\n\nservices:\n\n  apache:\n    image: apache2ssh:latest\n    ports:\n      - "+str(ports["db"])+":80\n      - "+str(ports["ssh"])+":22\n    volumes:\n      - /home/projecte/dockervol/"+docker["idname"]+":/var/www/html\n    deploy:\n      placement:\n        constraints: [node.hostname == pscluster-1]"


dockerstart = "/home/projecte/dockervol/"+docker["idname"]+".yml"

proc3 = os.system("mkdir /home/projecte/dockervol/"+docker["idname"])

if(docker["dockerproperty"]=="mariadb"):
	proc5 = os.system("chown 999:999 -r /home/projecte/dockervol/"+docker["idname"])
	dockercreate = open(dockerstart,'a')
	dockercreate.write(plantilla1)
	dockercreate.close()
	dockercreate = open(dockerstart,'r')
	prueba=dockercreate.read()
	proc = os.system("docker stack deploy --compose-file "+dockerstart+" "+docker["idname"])
elif(docker["dockerproperty"]=="postgres"):
	dockercreate = open(dockerstart,'a')
	dockercreate.write(plantilla2)
	dockercreate.close()
	dockercreate = open(dockerstart,'r')
	prueba=dockercreate.read()
	proc = os.system("docker stack deploy --compose-file "+dockerstart+" "+docker["idname"])
elif(docker["dockerproperty"]=="mysql"):
	proc5 = os.system("chown 999:999 -r /home/projecte/dockervol/"+docker["idname"])
	dockercreate = open(dockerstart,'a')
	dockercreate.write(plantilla3)
	dockercreate.close()
	dockercreate = open(dockerstart,'r')
	prueba=dockercreate.read()
	proc = os.system("docker stack deploy --compose-file "+dockerstart+" "+docker["idname"])
elif(docker["dockerproperty"]=="apache2"):
	proc4 = os.system(" cp /var/www/html/index.html /home/projecte/dockervol/"+docker["idname"]+"/index.php")
	dockercreate = open(dockerstart,'a')
	dockercreate.write(plantilla4)
	dockercreate.close()
	dockercreate = open(dockerstart,'r')
	prueba=dockercreate.read()
	proc = os.system("docker stack deploy --compose-file "+dockerstart+" "+docker["idname"])

if(proc==0):
	print 1
else:
	print 0

#proc = os.system("docker stack deploy --compose-file ../dockeryml/docker-compose.yml mysql")
#proc = os.system("ls -l")
#print(str(proc))
#print("hola, este es un script de python")
