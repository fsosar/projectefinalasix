<?php

namespace App\Controller;

use App\Entity\Activity;
use App\Entity\Member;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ActivityController extends Controller
{

    /**
     * @Route("/{_locale}/activity/search")
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $idMember = $helper->sanitize($request->request->get('idMember'));
        $wildcard = $helper->sanitize($request->request->get('wildcard'));
        $route = $helper->sanitize($request->request->get('route'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $serializer = new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $responseMsgError = [];

        $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            // Seguridad
            $token = $this->get('security.token_storage')->getToken()->getUser();
            if ($token->getIdMember() != $member->getIdMember() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
                throw new AccessDeniedException();
            }
            // END Seguridad

            if (empty($responseMsgError)) {
                $activity = $em->getRepository(Activity::class)->findBy([ 
                    'wildcard' => $wildcard,
                    'route' => $route
                    ], ['date' => 'DESC']
                );
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Error.');
        }

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError
            ]);
        } else {
            return JsonResponse::fromJsonString($serializer->serialize($activity, 'json'));
        }
        return $response;
    }

}
