<?php

namespace App\Controller\Backend;

use App\Entity\Activity;
use App\Entity\Member;
use App\Entity\MemberRole;
use App\Entity\MemberMeta;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class MemberController extends Controller
{

    /**
     * @Route("/{_locale}/dashboard/member/edit/{idMember}")
     */
    public function editAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $idMember = $helper->sanitize($request->get('idMember'));
        $idActivity = $helper->sanitize($request->get('history'));

        $dateNow = new DateTime('now');

        if ($idActivity) {
            $activity = $em->getRepository(Activity::class)->findOneBy(['idActivity' => $idActivity]);
            $activity = json_decode($activity->getRequest(), true);

            $member = new Member();
            if (isset($activity['name'])) {$member->setName($activity['name']);}
            if (isset($activity['username'])) {$member->setUsername($activity['username']);}
            if (isset($activity['surname'])) {$member->setSurname($activity['surname']);}
            if (isset($activity['secondSurname'])) {$member->setSecondSurname($activity['secondSurname']);}
            if (isset($activity['birthday'])) {$member->setBirthday($activity['birthday']);}
            if (isset($activity['gender'])) {$member->setGender($activity['gender']);}

        } else {
            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);
        }

        // Seguridad
        $token = $this->get('security.token_storage')->getToken()->getUser();
        if ($token->getIdMember() != $member->getIdMember() && !$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
            throw new AccessDeniedException();
        }
        // END Seguridad

        $roles = $em->getRepository(MemberRole::class)->findAll();

        $isChangeEmailRequest = false;
        $changeUsernameDate = $member->getChangeUsernameDate() + 259200;
        if ($changeUsernameDate >= $dateNow->getTimestamp()) {
            $isChangeEmailRequest = true;
        }

        return $this->render('backend/member/edit/html.html.twig', [
            'member' => $member,
            'roles' => $roles,
            'idActivity' => $idActivity,
            'isChangeEmailRequest' => $isChangeEmailRequest,
        ]);
    }

    /**
     * @Route("/{_locale}/dashboard/member/add")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository(MemberRole::class)->findBy([], ['idMemberRole' => 'DESC']);

        $member = new Member();

        return $this->render('backend/member/edit/html.html.twig', [
            'member' => $member,
            'roles' => $roles,
            'idActivity' => null,
            'isChangeEmailRequest' => false,
        ]);
    }

    /**
     * @Route("/{_locale}/dashboard/api/member/edit")
     */
    public function apiEditAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');
        $fileSaver = $this->get('app.file_saver');
        $response = new JsonResponse();

        $idMember = $helper->sanitize($request->request->get('idMember'));
        $name = $helper->sanitize($request->request->get('name'));
        $surname = $helper->sanitize($request->request->get('surname'));
        $secondSurname = $helper->sanitize($request->request->get('secondSurname'));
        $birthday = $helper->sanitize($request->request->get('birthday'));
        $password = $helper->sanitize($request->request->get('password'));
        $password_rep = $helper->sanitize($request->request->get('password_rep'));
        $prefijo = $helper->sanitize($request->request->get('prefijo'));
        $username = $helper->sanitize($request->request->get('username'));
        $gender = $helper->sanitize($request->request->get('gender'));
        $valido = $helper->sanitize($request->request->get('validado'));
        $delete = $helper->sanitize($request->request->get('deleted'));
        $role = $helper->sanitize($request->get('role'));
        $avatarUpload = $helper->sanitize($request->get('avatar'));
        $file = $request->files->get('upload');
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $accentedCharacters = 'àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ';
        $date = new DateTime('now');
        $responseMsgError = [];
        $responseMsgSuccess = [];

        try {
            // Seguridad
            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);
            $token = $this->get('security.token_storage')->getToken()->getUser();
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
                if ($token->getIdMember() != $member->getIdMember()) {
                    throw new AccessDeniedException();
                }
            }
            // END Seguridad

            // Validar campos
            $isCorrectForm = true;
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'Error');
            }

            if (!filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'El email introducido es invalido.');
            }

            if (!$idMember && empty($password_rep)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'Por favor, vuelve a introducir el password del usuario para asegurar que sea el deseado.');
            }

            if ($password != $password_rep) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'Cuidado! Los passwords no coinciden!');
            }

            if (!empty($name) && !preg_match('/^[a-zA-Z' . $accentedCharacters . '\s]*$/', $name)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'El nombre no cumple el formato deseado.');
            }

            if (!empty($surname) && !preg_match('/^[a-zA-Z' . $accentedCharacters . '\s]*$/', $surname)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'El primer apellido no cumple el formato deseado.');
            }
            if (!empty($secondSurname) && !preg_match('/^[a-zA-Z' . $accentedCharacters . '\s]*$/', $secondSurname)) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'El segundo apellido no cumple el formato deseado.');
            }

            if ($file) {
                $path = $file->getPathName();
                $whitelist_type = array('image/jpeg', 'image/png');

                if (function_exists('finfo_open')) { //(PHP >= 5.3.0, PECL fileinfo >= 0.1.0)
                    $fileinfo = finfo_open(FILEINFO_MIME_TYPE);

                    if (!in_array(finfo_file($fileinfo, $path), $whitelist_type)) {
                        $isCorrectForm = false;
                        array_push($responseMsgError, 'Formato de la imagen no valido. Sólo JPG y PNG');
                    }
                } else if (function_exists('mime_content_type')) { //supported (PHP 4 >= 4.3.0, PHP 5)
                    if (!in_array(mime_content_type($path), $whitelist_type)) {
                        $isCorrectForm = false;
                        array_push($responseMsgError, 'Formato de la imagen no valido. Sólo JPG y PNG');
                    }
                } else {
                    if (!@getimagesize($path)) { //@ - for hide warning when image not valid
                        $isCorrectForm = false;
                        array_push($responseMsgError, 'Formato de la imagen no valido. Sólo JPG y PNG');
                    }
                }
            }

            $memberEmail = $em->getRepository(Member::class)->findOneBy(['username' => $username, 'deleted' => 0]);
            if ($memberEmail && ($member->getUsername() != $username || !$member->getUsername())) {
                $isCorrectForm = false;
                array_push($responseMsgError, 'Ya existe un usuario registrado con este correo electrónico.');
            }

            if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
                $role = $em->getRepository(MemberRole::class)->findOneBy(['idMemberRole' => $role]);
                if (empty($role)) {
                    $isCorrectForm = false;
                    array_push($responseMsgError, 'Lo sentimos, ese rol no existe.');
                }
            }
            // END Validar campos

            if ($isCorrectForm) {

                if (!$idMember) {
                    $member = new Member();
                    $member->setCreationDate($date->getTimestamp());
                    $member->setValidated(1);
                    $member->setEmailValidated(1);
                    $member->setDeleted(0);
                    $member->setUsername($username);
                    $member->setIsActive(1);
                }

                $birthday = new DateTime($birthday);
                $member->setModificationDate($date->getTimestamp());
                $member->setName($name);
                $member->setSurname($surname);
                $member->setSecondSurname($secondSurname);
                $member->setBirthday($birthday->getTimestamp());
                $member->setGender($gender);

                if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
                    $member->setIdMemberRole($role);
                }

                // Cifrar la password
                if ($password && $password == $password_rep) {
                    $pwd = $encoder->encodePassword($member, $password);
                    $member->setPassword($pwd);
                }
                // END Cifrar la password

                // AVATAR
                if ($avatarUpload != '') {
                    $avatar = $member->getAvatar();
                    if (!file_exists('member/' . $idMember)) {
                        mkdir('member/' . $idMember, 0777, true);
                        if (!file_exists('member/' . $idMember . '/avatar')) {
                            mkdir('member/' . $idMember . '/avatar', 0777, true);
                        }
                    }
                    if ($avatar != null) {
                        if (file_exists('member/' . $idMember . '/avatar/' . $avatar)) {
                            unlink('member/' . $idMember . '/avatar/' . $avatar);
                        }
                    }
                    if ($avatarUpload == 'deleted') {
                        $member->setAvatar(null);
                    } else {
                        $numeroAleatorio = rand(1, 1000000);
                        $nombre = time() . '-' . $numeroAleatorio . '.png';
                        $ruta = 'member/' . $idMember . '/avatar/';
                        // RECOGE LA IMAGEN I LA GUARDA
                        $base_to_php = explode(',', $avatarUpload);
                        $data = base64_decode($base_to_php[1]);
                        $filePath = $ruta . $nombre;
                        file_put_contents($filePath, $data);
                        // END RECOGE LA IMAGEN I LA GUARDA
                        $member->setAvatar($nombre);
                    }
                }

                if (!is_null($file)) {
                    if ($fileSaver->checkS3()) {
                        if (!file_exists('temp/')) {
                            mkdir('temp');
                        }
                        // Generate a random name for the file but keep the extension
                        $filename = uniqid() . '.' . $file->getClientOriginalExtension();
                        $path = 'temp/';
                        $file->move($path, $filename);

                        switch ($fileSaver->saveFile('img/' . $filename)) {
                            case 200:
                                unlink($path . $filename);
                                break;
                            case 318:
                                array_push($responseMsgError, 'Por favor declara \'amazon.s3\', \'amazon.s3.profile\', \'amazon.s3.region\', \'amazon.s3.bucket\', en el fichero config.yml y ponle el nombre del bucket de Amazon a utilizar!');
                            case 502:
                                array_push($responseMsgError, 'Hubo algún error a la hora de subir el fichero, por favor, contacta con el técnico.');
                        }
                    } else {
                        $path = 'member/' . $idMember . '/avatar/';
                        $fileSaver->checkExistsLocal($path);
                        $filename = uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move($path, $filename);
                        if (!file_exists($path . $filename)) {
                            array_push($responseMsgError, 'Hubo algún error a la hora de subir el fichero, por favor, contacta con el técnico.');
                        } else {
                            $member->setAvatar($filename);
                        }
                    }
                }
                //  END AVATAR

                // Control cambio username
                if ($username != $member->getUsername() && !empty($idMember)) {
                    $emailSecretCode = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true) . $member->getIdMember());
                    $member->setEmailSecretCode($emailSecretCode);
                    $member->setChangeUsername($username);
                    $member->setChangeUsernameDate($date->getTimestamp());
                    $responseMsgSuccess = 'Comprueba tu correo electrónico, para terminal el cambio de email.';

                    $manager_mail->templeteOne([
                        'to' => $username,
                        'subject' => 'Email de verificación',
                        'body' =>
                            $this->renderView('templates/emails/member/verification_mail.html.twig', array(
                                'member' => $member,
                                'url' => $this->generateUrl('app_security_apiactivateusername', ['emailSecretCode' => $emailSecretCode, 'newUsername' => $username]),
                        ))
                    ]);
                }
                // END Control cambio username

                $em->persist($member);
                $em->flush();
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'No se pudo actualizar el usuario.');
        }

        $idMember = null;
        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
            $idMember = $member->getIdMember();
        }

        if (!$responseMsgSuccess) {
            $responseMsgSuccess = 'Usuario actualizado correctamente.';
        }
        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => $responseMsgSuccess,
            'idMember' => $idMember,
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError,
            ]);
        }
        return $response;
    }

    /**
     * @Route("/dashboard/member/delete")
     */
    public function deleteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $idMember = $helper->sanitize($request->request->get('idMember'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $response = new JsonResponse();
        $date = new DateTime('now');
        $responseMsgError = [];

        // Seguridad
        $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);
        $token = $this->get('security.token_storage')->getToken()->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMINISTRADOR')) {
            if ($token->getIdMember() != $member->getIdMember()) {
                throw new AccessDeniedException();
            }
        }
        // END Seguridad

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }
            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember, 'deleted' => 0]);
            if ($member) {
                $member->setDeactivationDate($date->getTimestamp());
                $member->setDeleted(1);
                $em->persist($member);
                $em->flush();
            } else {
                array_push($responseMsgError, 'No se pudo eliminar el usuario.');
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'No se pudo eliminar el usuario.');
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'El usuario se ha eliminado correctamente.',
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError,
            ]);
        }
        return $response;

    }

    /**
     * @Route("/dashboard/member/block")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function blockAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $idMember = $helper->sanitize($request->request->get('idMember'));
        $isActive = $helper->sanitize($request->request->get('isActive'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $response = new JsonResponse();
        $date = new DateTime('now');
        $responseMsgError = [];

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);
            if ($member) {
                $member->setLastActivity($date->getTimestamp());
                $member->setIsActive($isActive);
                $em->persist($member);
                $em->flush();
            } else {
                if ($isActive) {
                    array_push($responseMsgError, 'El usuario no se puede desbloquear.');
                } else {
                    array_push($responseMsgError, 'No se pudo bloquear el usuario.');
                }
            }
        } catch (Exception $e) {
            if ($isActive) {
                array_push($responseMsgError, 'El usuario no se puede desbloquear.');
            } else {
                array_push($responseMsgError, 'No se pudo bloquear el usuario.');
            }
        }

        if ($isActive) {
            $msg = 'El usuario se ha desbloqueado correctamente';
        } else {
            $msg = 'El usuario se ha bloqueado correctamente';
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => $msg,
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError,
            ]);
        }
        return $response;

    }

    /**
     * @Route("/verificar-email/{idMember}/{secretCode}")
     */
    public function verificarEmailAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idMember = $request->get('idMember');
        $secret_code = $request->get('secretCode');

        $member = $em->getRepository(Member::class)->findOneBy(['idMember' => $idMember]);
        $member_meta = $em->getRepository(MemberMeta::class)->findOneBy(['idMember' => $idMember]);
        if ($member && $member_meta->getEmailSecretCode() == $secret_code) {
            $member->setEmailValidated(1);
            $member_meta->setEmailSecretCode(null);
            $em->persist($member);
            $em->flush();
            $em->persist($member_meta);
            $em->flush();
        } else {
            dump('el codi secret no coincideix');
        }
        #mirar com reedirigir a una altra pàgina
        return $this->redirectToRoute('app_backend_member_edit', ['idMember' => $idMember]);
    }

    /**
     * @Route("/{_locale}/dashboard", defaults={"_locale": "es"}, name="dashboard")
     */
    public function dashboardAction(Request $request)
    {
        return $this->render('backend/member/dashboard.html.twig');
    }

    /**
     * @Route("/{_locale}/dashboard/member/search")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');

        $filterUsername = $helper->sanitize($request->get('filterUsername'));
        $filterRole = $helper->sanitize($request->get('filterRole'));
        $filterName = $helper->sanitize($request->get('filterName'));
        $emailValidated = $helper->sanitize($request->get('emailValidated'));
        $filterValidated = $helper->sanitize($request->get('filterValidated'));
        $filterIsActive = $helper->sanitize($request->get('filterIsActive'));
        $filterDeleted = $helper->sanitize($request->get('filterDeleted'));

        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));
        $responseMsgError = [];

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }

            if ($emailValidated == 'true') {$emailValidated = 1;}
            if ($filterValidated == 'true') {$filterValidated = 1;}
            if ($filterIsActive == 'true') {$filterIsActive = 1;}
            if ($filterDeleted == 'true') {$filterDeleted = 1;}

            $member = $em->getRepository(Member::class)->search([
                'username' => $filterUsername,
                'idMemberRole' => $filterRole,
                'name' => $filterName,
                'emailValidated' => $emailValidated,
                'validated' => $filterValidated,
                'isActive' => $filterIsActive,
                'deleted' => $filterDeleted,
            ]);

            $date2Minutes = new \DateTime('2 minutes ago');

        } catch (Exception $e) {
            array_push($responseMsgError, 'Error');
        }

        if (empty($responseMsgError)) {
            echo "Error";
            die();
        } else {
            return $this->render('backend/member/management/table.html.twig', [
                'member' => $member,
                'date2Minutes' => $date2Minutes->getTimestamp(),
            ]);
        }
    }

    /**
     * @Route("/{_locale}/dashboard/member/management")
     * @Security("has_role('ROLE_ADMINISTRADOR')")
     */
    public function managementAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $memberRole = $em->getRepository(MemberRole::class)->findAll();

        return $this->render('backend/member/management/html.html.twig', [
            'memberRole' => $memberRole,
        ]);
    }

}
