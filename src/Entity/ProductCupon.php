<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductCupon
 *
 * @ORM\Table(name="product_cupon", indexes={@ORM\Index(name="fk_product_cupon_product_idx", columns={"id_product"}), @ORM\Index(name="fk_product_cupon_cupon_idx", columns={"id_cupon"})})
 * @ORM\Entity
 */
class ProductCupon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_product_cupon", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idProductCupon;

    /**
     * @var \Cupons
     *
     * @ORM\ManyToOne(targetEntity="Cupons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cupon", referencedColumnName="id_cupon")
     * })
     */
    private $idCupon;

    /**
     * @var \Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_product", referencedColumnName="id_product")
     * })
     */
    private $idProduct;

    public function getIdProductCupon(): ?int
    {
        return $this->idProductCupon;
    }

    public function getIdCupon(): ?Cupons
    {
        return $this->idCupon;
    }

    public function setIdCupon(?Cupons $idCupon): self
    {
        $this->idCupon = $idCupon;

        return $this;
    }

    public function getIdProduct(): ?Product
    {
        return $this->idProduct;
    }

    public function setIdProduct(?Product $idProduct): self
    {
        $this->idProduct = $idProduct;

        return $this;
    }


}
