<?php

namespace App\Controller\Frontend;

use App\Entity\Member;
use App\Entity\MemberRole;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class MemberController extends Controller
{
    /**
     * @Route("/{_locale}", name="homepage" , defaults={"_locale": "en"})
     */
    public function indexAction(Request $request, AuthenticationUtils $authenticationUtils)
    {
        $loggedIn = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        $render = "";
        //$this->render('frontend/member/index.html.twig');
        if($loggedIn) $render = $this->redirectToRoute('dashboard');
        else $render = $this->redirectToRoute('app_login');
        
        return $render; 
    }

    /**
     * @Route("/{_locale}/member/registrar")
     */
    public function registrarAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();
        $helper = $this->get('app.helper');
        $manager_mail = $this->get('app.manager_mail');
        
        $username = $helper->sanitize($request->request->get('username'));
        $password = $helper->sanitize($request->request->get('password'));
        $csrfTtoken = $helper->sanitize($request->request->get('_csrf_token'));

        $responseMsgError = [];
        $response = new JsonResponse();
        $date = new DateTime('now');

        $name = strstr($username, '@', true);

        $member = $em->getRepository(Member::class)->checkExist($username);

        try {
            if (!$this->isCsrfTokenValid('authenticate', $csrfTtoken)) {
                array_push($responseMsgError, 'Error');
            }
            if (!strpos($username, "@") && !strpos($username, ".")) {
                array_push($responseMsgError, 'El email no es valido');
            }
            if ($member) {
                array_push($responseMsgError, 'El email ya se encuentra en uso.');
            }
            if (empty($responseMsgError)) {
                $member = new Member();

                $memberRole = $em->getRepository(MemberRole::class)->findOneBy(['idMemberRole' => 3]);
                $member->setIdMemberRole($memberRole);

                $member->setName($name);
                $member->setCreationDate($date->getTimestamp());
                $member->setModificationDate($date->getTimestamp());
                $member->setEmailValidated(1);
                $member->setValidated(1);
                $member->setDeleted(0);
                $member->setIsActive(1);

                $pwd = $encoder->encodePassword($member, $password);
                $member->setPassword($pwd);

                $emailSecretCode = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true) . $member->getIdMember());
                $member->setEmailSecretCode($emailSecretCode);
                $member->setUsername($username);

                $em->persist($member);
                $em->flush();
                /*
                $manager_mail->templeteOne([
                    'to' => $username,
                    'subject' => 'Email de verificación',
                    'body' =>
                        $this->renderView('templates/emails/member/register_member.html.twig', [
                            'url' => $this->generateUrl('app_security_apiactivate', ['secretCode' => $emailSecretCode]),
                    ])
                ]);
                */
            }
        } catch (Exception $e) {
            array_push($responseMsgError, 'Hubo un error al registrarte.');
        }

        $response->setStatusCode(200);
        $response->setData([
            'response' => 'success',
            'msg' => 'Enhorabuena, te registraste correctamente, comprueba el email para verificar el registro!'
        ]);

        if (!empty($responseMsgError)) {
            $response->setStatusCode(500);
            $response->setData([
                'response' => 'error',
                'msg' => $responseMsgError
            ]);
        }
        return $response;
    }

}
