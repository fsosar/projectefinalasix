<?php

namespace App\Repository;

use App\Entity\Member;

use Doctrine\ORM\EntityRepository;

class MemberRepository extends EntityRepository
{

    public function checkExist($username)
    {
        $qb = $this->createQueryBuilder('m');

        $qb->andWhere('m.username like :username')->setParameter('username', '%' . $username . '%');
        $qb->andWhere('m.deleted = 0');

        return $qb->getQuery()->execute();
    }

    public function search($parameter)
    {
        $username = $parameter["username"];
        $idMemberRole = $parameter["idMemberRole"];
        $name = $parameter["name"];
        $emailValidated = $parameter["emailValidated"];
        $validated = $parameter["validated"];
        $isActive = $parameter["isActive"];
        $deleted = $parameter["deleted"];

        $qb = $this->createQueryBuilder('m');

        if ($username) {
            $qb->andWhere('m.username like :username')->setParameter('username', '%' . $username . '%');
        }

        if ($name) {
            $qb->andWhere('m.name like :name')->setParameter('name', '%' . $name . '%');
        }

        if ($idMemberRole) {
            $qb->andWhere('m.idMemberRole = :idMemberRole')->setParameter('idMemberRole', $idMemberRole);
        }

        if ($emailValidated) {
            $qb->andWhere('m.emailValidated = :emailValidated')->setParameter('emailValidated', $emailValidated);
        }

        if ($isActive) {
            $qb->andWhere('m.isActive = :isActive')->setParameter('isActive', $isActive);
        }

        if ($validated) {
            $qb->andWhere('m.validated = :validated')->setParameter('validated', $validated);
        }

        if ($deleted) {
            $qb->andWhere('m.deleted = :deleted')->setParameter('deleted', $deleted);
        }

        return $qb->getQuery()->execute();
    }

}
